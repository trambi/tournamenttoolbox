# TournamentToolBox

A tool box to rescue tournament organizers :

- compute extra columns for games such as points (implemented);
- rank opponents given games (implemented),
- pair opponents given rankings (implemented - options to confirm)

```mermaid
graph LR;
    games((Games))-->compute[Compute extra fields]
    settings((Settings))-->compute
    compute-->games_with_fields>Games with extra fields]
    games_with_fields-->rank[Rank]
    settings-->rank
    rank-->rankings(Rankings)
    pair[Pair]-...->pairings((Pairings))
    rankings-....->pair
    settings-...->pair
    games_with_fields-...->pair
```

The goal is to have just to download one executable with all the tools.

## Values

- **Simplicity**: we try to make the tournament tool box as simple as possible.
- **Effiencty**: we try to do fast the job.

## Getting Started

### Compute extra columns for games

Run `./tournamenttoolbox compute examples/games_to_compute.csv` compute the fields `td_net_1`, `td_net_2`, `points_1` and `points_2` for each game in the file `examples/games_to_compute.csv` and write the games in file `"games_modified.csv"`.

### Rank opponents

run `./tournamenttoolbox rank examples/games_to_rank.csv Main` rank the teams using games in the file `examples/games_to_rank.csv` using the ranking named `Main` (defined in `.tournamenttoolbox.json`) and write the ranking in the file `ranking.csv`.

### Pair opponents

run `./tournamenttoolbox pair id examples/ranking_to_pair.csv examples/games_to_pair.csv` will pair the teams with ranking in the file `examples/ranking_to_pair.csv` excluding games already played defined in the file `examples/games_to_pair.csv` and write the ranking in the file `pairing.csv`.

### Prerequisites

No prerequisites to execute.

Need Golang installed to develop.

### Installing

Download the executable for your operating system.

## Running the tests

Clone the repository and

```bash
go test ./cmd ./pkg/...
```

## Built with

- [Golang] (https://golang.org/) - Go Language
- [urfave/cli go library] (https://github.com/urfave/cli) - Library to create CLI executable

```bash
go build -o tournamenttoolbox ./cmd
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

Here is a summary of the package dependencies.

```mermaid
graph TD;
    subgraph CLI
        cmd[cmd/main]
    end
    subgraph Core
        compute[pkg/compute]
        evaluator[pkg/evaluator]
        rank[pkg/rank]
        settings[pkg/settings]
        pair[pkg/pair]
    end
    subgraph "Json IO"
        json_settings[pkg/json/settings]
    end
    subgraph "CSV IO"
        csv[pkg/csv]
        csv_pair[pkg/csv/pair]
        csv_rank[pkg/csv/rank]
    end
    csv_pair-->pair
    csv_rank-->rank
    compute-->evaluator
    settings-->compute
    settings-->rank
    json_settings-->rank
    json_settings-->compute
    json_settings-->settings

    cmd-->compute
    cmd-->csv
    cmd-->pair
    cmd-->csv_pair
    cmd-->rank
    cmd-->csv_rank
    cmd-->json_settings
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/trambi/tournamenttoolbox/tags).

## Authors

- **Bertrand Madet** - _Initial work_ - [Trambi](https://gitlab.com/Trambi)

See also the list of [contributors](https://gitlab.com/trambi/tournamenttoolbox/-/graphs/master) who participated in this project.

## License

This project is licensed under the Apache v2 License - see the [LICENSE.md](LICENSE.md) file for details
