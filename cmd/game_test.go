// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"io/ioutil"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGamesFromCsvFile(t *testing.T) {
	expectedgames := [][]string{
		{"id_1", "team_1", "td_1", "id_2", "team_2", "td_2", "td_net_1", "td_net_2", "points_1", "points_2"},
		{"1", "first_team", "2", "3", "third_team", "1", "2", "-2", "3", "0"},
		{"2", "second_team", "1", "4", "last_team", "0", "1", "-1", "3", "0"},
	}
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	games, err := gamesFromCsvFile(filepath.Join(relPath, "..", "test", "games_to_rank.csv"))
	if err != nil {
		t.Errorf("nexpected error: %v\n", err.Error())
	} else if cmp.Equal(games, expectedgames) == false {
		t.Errorf("games %v are different from expected %v", games, expectedgames)
	}
}

func TestGamesToCsvFile(t *testing.T) {
	expectedgames := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", "3", "1", "2", "third_team", "0", "0"},
		{"3", "second_team", "3", "2", "4", "last_team", "0", "1"},
	}
	outputDir, _ := ioutil.TempDir("", "games_")
	outputPath := filepath.Join(outputDir, "games_new.csv")
	//defer os.RemoveAll(outputDir)

	err := gamesToCsvFile(expectedgames, outputPath)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	var games [][]string
	games, err = gamesFromCsvFile(outputPath)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(games, expectedgames) == false {
		t.Errorf("games %v are different from expected %v", games, expectedgames)
	}
}
