// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/trambi/tournamenttoolbox/pkg/compute"
	"gitlab.com/trambi/tournamenttoolbox/pkg/csv"
	csvpair "gitlab.com/trambi/tournamenttoolbox/pkg/csv/pair"
	csvrank "gitlab.com/trambi/tournamenttoolbox/pkg/csv/rank"
	jsonsettings "gitlab.com/trambi/tournamenttoolbox/pkg/json/settings"
	"gitlab.com/trambi/tournamenttoolbox/pkg/pair"
	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
)

func main() {
	app := &cli.App{
		Name:    "tournamenttoolbox",
		Usage:   "A tool box to rescue tournament organizers",
		Version: Version,
		Flags: []cli.Flag{
			&cli.PathFlag{
				Name:  "config, c",
				Usage: "Load configuration from `FILE`",
				Value: ".tournamenttoolbox.json",
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "compute",
				Usage: "compute columns for each games using configuration and games file",
				Action: func(c *cli.Context) error {
					return ComputeFromFiles(c.Path("config"), c.Args().First(), c.Path("output"))
				},
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:  "output, o",
						Usage: "Output filepath to write games",
						Value: "games_modified.csv",
					},
				},
				ArgsUsage: "[Path of games in csv]",
			},
			{
				Name:  "rank",
				Usage: "Create a ranking using configuration and games file",
				Action: func(c *cli.Context) error {
					return RankFromFiles(c.Path("config"), c.Args().First(), c.Args().Get(1), c.Path("output"))
				},
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:  "output, o",
						Usage: "Output filepath to write ranking",
						Value: "ranking.csv",
					},
				},
				ArgsUsage: "[Path of games in csv] [Ranking name]",
			},
			{
				Name:  "pair",
				Usage: "Propose a pair using ranking and already played games",
				Action: func(c *cli.Context) error {
					return PairFromFiles(
						c.Args().First(),
						c.Args().Get(1),
						c.Args().Get(2),
						c.Path("output"))
				},
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:  "output, o",
						Usage: "Output filepath to write pairing",
						Value: "pairing.csv",
					},
				},
				ArgsUsage: "[Id prefix] [Path of ranking in csv] [Path of games in csv]",
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

// RankFromFiles create a ranking file at outputPath
// given settings in file named SettingsPath and games in file named gamesPath
func RankFromFiles(settingsPath string, gamesPath string, name string, outputPath string) error {
	var games [][]string
	var ranking rank.Ranking
	settings, err := jsonsettings.FromJSONFile(settingsPath)
	if err != nil {
		return fmt.Errorf("error during loading settings from json file: %s", err.Error())
	}
	rankingSettings, existInMap := settings.Rankings[name]
	if !existInMap {
		return fmt.Errorf("unable to find ranking \"%v\" in %v ", name, settingsPath)
	}

	games, err = gamesFromCsvFile(gamesPath)
	if err != nil {
		return fmt.Errorf("error during loading games from csv file: %s", err.Error())
	}

	ranking, err = rank.Rank(rankingSettings, games)
	if err != nil {
		return fmt.Errorf("error during computing ranking: %s", err.Error())
	}
	err = csv.ToFile(csvrank.RankingCsv{R: ranking}, outputPath)
	if err != nil {
		return fmt.Errorf("error during writing ranking to csv file: %s", err.Error())
	}
	return nil
}

// ComputeFromFiles create a games file with extra computed columns at outputPath
// given settings in file named SettingsPath and games in file named gamesPath
func ComputeFromFiles(settingsPath string, gamesPath string, outputPath string) error {
	var games, newgames [][]string
	settings, err := jsonsettings.FromJSONFile(settingsPath)
	if err != nil {
		return fmt.Errorf("error during loading settings from json file: %s", err.Error())
	}
	games, err = gamesFromCsvFile(gamesPath)
	if err != nil {
		return fmt.Errorf("error during loading games from csv file: %s", err.Error())
	}

	newgames, err = compute.Compute(settings.ComputedFields, games[0], games[1:])
	if err != nil {
		return fmt.Errorf("error during computing fields: %s", err.Error())
	}
	result := append([][]string{}, compute.AppendComputedFieldsToHeaders(settings.ComputedFields, games[0]))
	result = append(result, newgames...)

	err = gamesToCsvFile(result, outputPath)
	if err != nil {
		return fmt.Errorf("error during writing newgames to csv file: %s", err.Error())
	}
	return nil
}

// PairFromFiles create a pairing file at outputPath
// given prefix of identifier named idPrefix
// and ranking in file named rankingPath
// and games in file named gamesPath
func PairFromFiles(
	idPrefix string,
	rankingPath string,
	gamesPath string,
	outputPath string) error {
	rankingRowsCsv := &csvrank.RankingRowsCsv{}
	err := csv.FromFile(rankingRowsCsv, rankingPath)
	if err != nil {
		return fmt.Errorf("error during loading ranking from csv file: %s", err.Error())
	}
	var games [][]string
	games, err = gamesFromCsvFile(gamesPath)
	if err != nil {
		return fmt.Errorf("error during loading games from csv file: %s", err.Error())
	}
	ranking := rankingRowsCsv.Rows
	var id1, id2 int
	id1, id2, err = csvpair.GetIndexesFromHeaders(games[0], idPrefix)
	if err != nil {
		return fmt.Errorf("unable to find field with prefix \"%s\" from games file: %s", idPrefix, err.Error())
	}

	var toPair []string
	for _, row := range ranking {
		toPair = append(toPair, row.ID)
	}
	var forbiddenPairings []pair.Pairing
	for _, game := range games {
		forbiddenPairings = append(forbiddenPairings, pair.Pairing{
			Opponent1: game[id1],
			Opponent2: game[id2]})
	}
	var pairings []pair.Pairing
	pairings, err = pair.Pair(toPair, forbiddenPairings)
	if err != nil {
		return fmt.Errorf("error during pairing: %s", err.Error())
	}
	csv.ToFile(csvpair.PairingsCsv{Pairings: pairings}, outputPath)
	return nil
}
