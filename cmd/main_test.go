// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/csv"
	csvpair "gitlab.com/trambi/tournamenttoolbox/pkg/csv/pair"
	"gitlab.com/trambi/tournamenttoolbox/pkg/pair"
)

func TestRankFromFiles(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "ranking_")
	outputPath := filepath.Join(outputDir, "ranking_for_test.csv")
	defer os.RemoveAll(outputDir)
	settingsPath := filepath.Join(relPath, "..", "test", ".tournamenttoolbox.json")
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	rankingName := "Main"
	err := RankFromFiles(settingsPath, gamesPath, rankingName, outputPath)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
}

func TestRankFromFilesWithNonExistentgamesFile(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "ranking_")
	outputPath := filepath.Join(outputDir, "ranking_for_test.csv")
	defer os.RemoveAll(outputDir)
	settingsPath := filepath.Join(relPath, "..", "test", ".tournamenttoolbox.json")
	gamesPath := "nonexistent_games.csv"
	rankingName := "Main"
	expectedError := "error during loading games from csv file: unable to read file [open nonexistent_games.csv: no such file or directory]"
	err := RankFromFiles(settingsPath, gamesPath, rankingName, outputPath)
	if err == nil {
		t.Error("Expected error does not occur\n")
	} else if err.Error() != expectedError {
		t.Errorf("Unexpected error %v (expected %v)\n", err.Error(), expectedError)
	}
}

func TestRankFromFilesWithNonExistentSettingsFile(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "ranking_")
	outputPath := filepath.Join(outputDir, "ranking_for_test.csv")
	defer os.RemoveAll(outputDir)
	settingsPath := "nonexistent_settings.json"
	rankingName := "Main"
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	expectedError := "error during loading settings from json file: unable to read file [open nonexistent_settings.json: no such file or directory]"
	err := RankFromFiles(settingsPath, gamesPath, rankingName, outputPath)
	if err == nil {
		t.Error("Expected error does not occur\n")
	} else if err.Error() != expectedError {
		t.Errorf("Unexpected error %v (expected %v)\n", err.Error(), expectedError)
	}
}

func TestRankFromFilesWithInvalidOutputPath(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputPath := filepath.Join("not_existing_path", "ranking_for_test.csv")
	settingsPath := filepath.Join(relPath, "..", "test", ".tournamenttoolbox.json")
	rankingName := "Main"
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	err := RankFromFiles(settingsPath, gamesPath, rankingName, outputPath)
	expectedError := "error during writing ranking to csv file: unable to write in such path [not_existing_path/ranking_for_test.csv]: open not_existing_path/ranking_for_test.csv: no such file or directory"
	if err == nil {
		t.Error("Expected error does not occur\n")
	} else if err.Error() != expectedError {
		t.Errorf("Unexpected error %v (expected %v)\n", err.Error(), expectedError)
	}
}

// TestRankUnvalidRankingName tests that
// Given valid settings path, valid games path, valid output path and unvalid ranking name
// rank must return an error "Not existing ranking name"
func TestRankFromFilesUnvalidRankingName(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "ranking_")
	outputPath := filepath.Join(outputDir, "ranking_for_test.csv")
	defer os.RemoveAll(outputDir)
	settingsPath := filepath.Join(relPath, "..", "test", ".tournamenttoolbox.json")
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	invalidRankingName := "invalid ranking name"
	err := RankFromFiles(settingsPath, gamesPath, invalidRankingName, outputPath)
	expectedError := fmt.Sprintf("unable to find ranking \"%v\" in %v ", invalidRankingName, settingsPath)
	if err == nil {
		t.Error("Expected error does not occur\n")
	} else if err.Error() != expectedError {
		t.Errorf("Unexpected error %v (expected %v)\n", err.Error(), expectedError)
	}
}

func TestComputeFromFiles(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "compute_")
	outputPath := filepath.Join(outputDir, "computed_games.csv")
	defer os.RemoveAll(outputDir)
	settingsPath := filepath.Join(relPath, "..", "test", ".tournamenttoolbox.json")
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_compute.csv")
	err := ComputeFromFiles(settingsPath, gamesPath, outputPath)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
}

func TestPairFromFiles(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "pair_")
	outputPath := filepath.Join(outputDir, "pairing.csv")
	defer os.RemoveAll(outputDir)
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	rankPath := filepath.Join(relPath, "..", "test", "ranking_to_pair.csv")
	err := PairFromFiles("id", rankPath, gamesPath, outputPath)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	pairingsCsv := &csvpair.PairingsCsv{}
	csv.FromFile(pairingsCsv, outputPath)
	expected := []pair.Pairing{
		{Opponent1: "1", Opponent2: "2"},
		{Opponent1: "3", Opponent2: "4"},
	}
	if cmp.Equal(pairingsCsv.Pairings, expected) == false {
		t.Errorf("%v should be equal to : %v\n", pairingsCsv.Pairings, expected)
	}
}
func TestPairFromFilesWithInvalidRankPath(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "pair_")
	outputPath := filepath.Join(outputDir, "pairing.csv")
	defer os.RemoveAll(outputDir)
	gamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	invalidRankPath := filepath.Join(relPath, "invalid_ranking.csv")
	err := PairFromFiles("id", invalidRankPath, gamesPath, outputPath)
	expected := "error during loading ranking from csv file"
	if err == nil {
		t.Errorf("Should return an error\n")
	}
	if strings.HasPrefix(err.Error(), expected) == false {
		t.Errorf("Should return error: %v but got %v\n", expected, err.Error())
	}
}

func TestPairFromFilesWithGamesWithoutID(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "pair_")
	outputPath := filepath.Join(outputDir, "pairing.csv")
	defer os.RemoveAll(outputDir)
	invalidGamesPath := filepath.Join(relPath, "..", "test", "games_to_rank.csv")
	rankPath := filepath.Join(relPath, "..", "test", "ranking_to_pair.csv")
	err := PairFromFiles("not_present_prefix", rankPath, invalidGamesPath, outputPath)
	expected := "unable to find field with prefix \"not_present_prefix\" from games file"
	if err == nil {
		t.Errorf("Should return an error: %s\n", expected)
	}
	if strings.HasPrefix(err.Error(), expected) == false {
		t.Errorf("Should return error: %v but got %v\n", expected, err.Error())
	}
}

func TestPairFromFilesWithInvalidGamesPath(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	outputDir, _ := ioutil.TempDir("", "pair_")
	outputPath := filepath.Join(outputDir, "pairing.csv")
	defer os.RemoveAll(outputDir)
	invalidGamesPath := filepath.Join(relPath, "invalid_games.csv")
	rankPath := filepath.Join(relPath, "..", "test", "ranking_to_pair.csv")
	err := PairFromFiles("id", rankPath, invalidGamesPath, outputPath)
	expected := "error during loading games from csv file"
	if err == nil {
		t.Errorf("Should return an error\n")
	}
	if strings.HasPrefix(err.Error(), expected) == false {
		t.Errorf("Should return error: %v but got %v\n", expected, err.Error())
	}
}
