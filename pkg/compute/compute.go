// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import (
	"fmt"
	"strings"

	"gitlab.com/trambi/tournamenttoolbox/pkg/evaluator"
)

// Compute for ComputedField computes the value for row
func computeOnegame(fields []ComputedField, headers []string, row []string) ([]string, error) {
	isTrue := false
	context, err := contextFromHeadersAndRow(headers, row)
	if err != nil {
		return []string{}, fmt.Errorf("error in creating context from headers and rows: %v", err.Error())
	}
	result := make([]string, len(row)+len(fields))
	copy(result, row)

	for i, field := range fields {
		var newRow string
		for _, statement := range field.Statements {
			isTrue, _ = statement.Clause.IsTrue(row)
			if isTrue {
				newRow, err = evaluator.Evaluate(statement.ValueIfTrue, context)
				if err != nil {
					return []string{}, err
				}
				break
			}
		}
		if len(newRow) == 0 {
			newRow, err = evaluator.Evaluate(field.DefaultValue, context)
			if err != nil {
				return []string{}, err
			}
		}
		result[len(row)+i] = newRow
	}
	return result, nil
}

// Compute for ComputedField computes the value for many rows
func Compute(fields []ComputedField, headers []string, rows [][]string) ([][]string, error) {
	for _, field := range fields {
		for _, statement := range field.Statements {
			statement.Clause.Init(headers)
		}
	}
	var result [][]string
	for index, row := range rows {
		tmp, err := computeOnegame(fields, headers, row)
		if err != nil {
			return result, fmt.Errorf("error during computation of row #%d : %v", index, err.Error())
		}
		result = append(result, tmp)
	}
	return result, nil
}

func contextFromHeadersAndRow(headers []string, row []string) (map[string]string, error) {
	context := make(map[string]string)
	length := len(headers)
	if length != len(row) {
		return context, fmt.Errorf("trying to create context from headers (%v) and row (%v) of different sizes", length, len(row))
	}
	for i := 0; i < length; i++ {
		context[strings.Trim(headers[i], " ")] = strings.Trim(row[i], " ")
	}
	return context, nil
}

// AppendComputedFieldsToHeaders concatenate headers with name of fields
func AppendComputedFieldsToHeaders(fields []ComputedField, headers []string) []string {
	var result = make([]string, len(headers))
	copy(result, headers)
	for _, field := range fields {
		result = append(result, field.Name)
	}
	return result
}
