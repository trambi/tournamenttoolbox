// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestComputedFieldDefault(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	row := []string{"0", "1"}
	expected := append(row, "0")
	equalClause := EqualClause{Left: "td_1", Right: "td_2"}
	greaterClause1 := GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	equalClause.Init(headers)
	greaterClause1.Init(headers)
	field := ComputedField{
		Name:         "points_1",
		DefaultValue: "0",
		Statements: []Statement{
			{Clause: &equalClause, ValueIfTrue: "1"},
			{Clause: &greaterClause1, ValueIfTrue: "3"},
		},
	}
	result, err := computeOnegame([]ComputedField{field}, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v are different from expected %v", result, expected)
	}
}

func TestComputedFieldStatements(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	rows := [][]string{{"0", "0"}, {"1", "0"}}
	expected := [][]string{{"0", "0", "1"}, {"1", "0", "3"}}
	equalClause := EqualClause{
		Left:  "td_1",
		Right: "td_2",
	}
	greaterClause1 := GreaterClause{
		ToBeGreater: "td_1",
		ToBeLesser:  "td_2",
	}
	equalClause.Init(headers)
	greaterClause1.Init(headers)
	fields := []ComputedField{{
		Name:         "points_1",
		DefaultValue: "0",
		Statements: []Statement{
			{Clause: &equalClause, ValueIfTrue: "1"},
			{Clause: &greaterClause1, ValueIfTrue: "3"},
		},
	}}
	for index, row := range rows {
		result, err := computeOnegame(fields, headers, row)
		if err != nil {
			t.Errorf("Unexpected error: %v\n", err.Error())
		} else if cmp.Equal(result, expected[index]) == false {
			t.Errorf("result %v is different from expected %v", result, expected[index])
		}
	}
}

func TestCompute(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	rows := [][]string{{"0", "0"}, {"1", "0"}}
	expected := [][]string{{"0", "0", "1"}, {"1", "0", "3"}}
	equalClause := EqualClause{Left: "td_1", Right: "td_2"}
	greaterClause1 := GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	fields := []ComputedField{{
		Name:         "points_1",
		DefaultValue: "0",
		Statements: []Statement{
			{Clause: &equalClause, ValueIfTrue: "1"},
			{Clause: &greaterClause1, ValueIfTrue: "3"},
		},
	}}

	result, err := Compute(fields, headers, rows)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestComputeFourFields(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	rows := [][]string{{"0", "0"}, {"1", "0"}, {"2", "0"}}
	expected := [][]string{{"0", "0", "0", "0", "1", "1"}, {"1", "0", "1", "-1", "3", "0"}, {"2", "0", "2", "-2", "3", "0"}}
	equalClause := EqualClause{Left: "td_1", Right: "td_2"}
	greaterClause1 := GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	greaterClause2 := GreaterClause{ToBeGreater: "td_2", ToBeLesser: "td_1"}
	fields := []ComputedField{
		{Name: "td_net_1", DefaultValue: "{td_1}-{td_2}", Statements: []Statement{}},
		{Name: "td_net_2", DefaultValue: "{td_2}-{td_1}", Statements: []Statement{}},
		{Name: "points_1", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
				{Clause: &greaterClause1, ValueIfTrue: "3"},
			},
		},
		{Name: "points_2", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
				{Clause: &greaterClause2, ValueIfTrue: "3"},
			},
		},
	}

	result, err := Compute(fields, headers, rows)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestComputedDirectFieldOppositeNumber(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	row := []string{"2", "1"}
	expected := append(row, "1")
	fields := []ComputedField{{
		Name:         "td_net_1",
		DefaultValue: "{td_1}-{td_2}",
		Statements:   []Statement{},
	}}
	result, err := computeOnegame(fields, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected 1", result)
	}
}

func TestComputeOnegameFourFields(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	row := []string{"2", "1"}
	expected := append(row, "1", "-1", "3", "0")
	equalClause := EqualClause{
		Left:  "td_1",
		Right: "td_2",
	}
	greaterClause1 := GreaterClause{
		ToBeGreater: "td_1",
		ToBeLesser:  "td_2",
	}
	greaterClause2 := GreaterClause{
		ToBeGreater: "td_2",
		ToBeLesser:  "td_1",
	}
	equalClause.Init(headers)
	greaterClause1.Init(headers)
	greaterClause2.Init(headers)
	fields := []ComputedField{
		{Name: "td_net_1", DefaultValue: "{td_1}-{td_2}", Statements: []Statement{}},
		{Name: "td_net_2", DefaultValue: "{td_2}-{td_1}", Statements: []Statement{}},
		{Name: "points_1", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
				{Clause: &greaterClause1, ValueIfTrue: "3"},
			},
		},
		{Name: "points_2", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
				{Clause: &greaterClause2, ValueIfTrue: "3"},
			},
		},
	}
	result, err := computeOnegame(fields, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestComputeOnegameOneDecimalField(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	row := []string{"1", "1"}
	expected := append(row, "0.5")
	equalClause := EqualClause{
		Left:  "td_1",
		Right: "td_2",
	}
	greaterClause1 := GreaterClause{
		ToBeGreater: "td_1",
		ToBeLesser:  "td_2",
	}
	equalClause.Init(headers)
	greaterClause1.Init(headers)
	fields := []ComputedField{
		{
			Name: "points_1", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "0.5"},
				{Clause: &greaterClause1, ValueIfTrue: "1"},
			},
		},
	}
	result, err := computeOnegame(fields, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestComputeWithDecimalInEqualClause(t *testing.T) {
	headers := []string{"ratio_1", "ratio_2"}
	row := []string{"0.75", "0.75"}
	expected := append(row, "1")
	equalClause := EqualClause{
		Left:  "ratio_1",
		Right: "ratio_2",
	}
	equalClause.Init(headers)
	fields := []ComputedField{
		{
			Name: "points_1", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
			},
		},
	}
	result, err := computeOnegame(fields, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestComputeWithDecimalInGreaterClause(t *testing.T) {
	headers := []string{"ratio_1", "ratio_2"}
	row := []string{"0.85", "0.75"}
	expected := append(row, "1")
	greaterClause := GreaterClause{
		ToBeGreater: "ratio_1",
		ToBeLesser:  "ratio_2",
	}
	greaterClause.Init(headers)
	fields := []ComputedField{
		{
			Name: "points_1", DefaultValue: "0",
			Statements: []Statement{
				{Clause: &greaterClause, ValueIfTrue: "1"},
			},
		},
	}
	result, err := computeOnegame(fields, headers, row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestContextFromHeadersAndRowDifferentLength(t *testing.T) {
	context, err := contextFromHeadersAndRow([]string{"column_1"}, []string{"1", "2"})
	expected := "trying to create context from headers (1) and row (2) of different sizes"
	if err == nil {
		t.Error("Should return an error")
	} else if err.Error() != expected {
		t.Errorf("Unexpected error \"%v\" \"%v\" was expected", err.Error(), expected)
	} else if len(context) != 0 {
		t.Errorf("Resulting context should be empty: %v", context)
	}
}

func TestContextFromHeadersAndRow(t *testing.T) {
	context, err := contextFromHeadersAndRow([]string{"column_1", "column_2", "column_3"}, []string{"1", "a", "2"})
	expected := make(map[string]string)
	expected["column_1"] = "1"
	expected["column_3"] = "2"
	expected["column_2"] = "a"
	if err != nil {
		t.Errorf("Unexpected error \"%v\"", err.Error())
	} else if cmp.Equal(context, expected) == false {
		t.Errorf("Resulting context :%v should be equal to %v", context, expected)
	}
}

func TestContextFromHeadersAndRowWithSpaceInValue(t *testing.T) {
	context, err := contextFromHeadersAndRow([]string{"column_1", "column_a", "column_2", "column_3"}, []string{"1", "a", " 2", "3 "})
	expected := make(map[string]string)
	expected["column_1"] = "1"
	expected["column_2"] = "2"
	expected["column_3"] = "3"
	expected["column_a"] = "a"
	if err != nil {
		t.Errorf("Unexpected error \"%v\"", err.Error())
	} else if cmp.Equal(context, expected) == false {
		t.Errorf("Resulting context :%v should be equal to %v", context, expected)
	}
}

func TestContextFromHeadersAndRowWithSpaceInKey(t *testing.T) {
	context, err := contextFromHeadersAndRow([]string{"column_1", "column_a", " column_2", "column_3 "}, []string{"1", "a", "2", "3"})
	expected := make(map[string]string)
	expected["column_1"] = "1"
	expected["column_2"] = "2"
	expected["column_3"] = "3"
	expected["column_a"] = "a"
	if err != nil {
		t.Errorf("Unexpected error \"%v\"", err.Error())
	} else if cmp.Equal(context, expected) == false {
		t.Errorf("Resulting context :%v should be equal to %v", context, expected)
	}
}

func TestAppendComputedFieldsToHeaders(t *testing.T) {
	headers := []string{"td_1", "td_2"}
	fields := []ComputedField{
		{Name: "td_net_1", DefaultValue: "{td_1}-{td_2}", Statements: []Statement{}},
		{Name: "td_net_2", DefaultValue: "{td_2}-{td_1}", Statements: []Statement{}},
	}
	expected := append(headers, "td_net_1", "td_net_2")
	result := AppendComputedFieldsToHeaders(fields, headers)
	if cmp.Equal(result, expected) == false {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}
