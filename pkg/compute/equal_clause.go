// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import (
	"errors"
	"fmt"
	"math/big"
	"strings"
)

// EqualClause describe an equal clause true when left is equal to right
type EqualClause struct {
	Left        string
	Right       string
	Initialized bool
	LeftIndex   int
	RightIndex  int
}

// Init for EqualClause initiate index from headers
func (equalClause *EqualClause) Init(headers []string) error {
	found := 0
	for index, value := range headers {
		trimedValue := strings.Trim(value, " ")
		if trimedValue == equalClause.Left {
			equalClause.LeftIndex = index
			found++
		} else if trimedValue == equalClause.Right {
			equalClause.RightIndex = index
			found++
		}
		if found == 2 {
			equalClause.Initialized = true
			break
		}
	}
	if found != 2 {
		return fmt.Errorf("%v, %v not found in headers", equalClause.Left, equalClause.Right)
	}
	return nil
}

// IsTrue for EqualClause returns true when the value of LeftIndex is equal to the value of RightIndex
func (equalClause *EqualClause) IsTrue(row []string) (bool, error) {
	if !equalClause.Initialized {
		return false, errors.New("EqualClause not initialized")
	}
	value1 := new(big.Float)
	value2 := new(big.Float)
	_, _, err1 := value1.Parse(row[equalClause.LeftIndex], 10)
	_, _, err2 := value2.Parse(row[equalClause.RightIndex], 10)
	if err1 == nil && err2 == nil {
		return value1.Cmp(value2) == 0, nil
	}
	return false, fmt.Errorf("error on field1: %v and on field2: %v", err1, err2)
}

// String allow to convert an EqualClause to string
func (equalClause *EqualClause) String() string {
	return fmt.Sprintf("Left: %v Right: %v", equalClause.Left, equalClause.Right)
}
