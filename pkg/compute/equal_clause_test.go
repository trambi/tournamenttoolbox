// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import "testing"

func TestEqualClauseIsTrueWithLesser(t *testing.T) {
	equalClause := EqualClause{Left: "first", Right: "second"}
	row := []string{"0", "1"}
	equalClause.Init([]string{"first", "second"})
	result, err := equalClause.IsTrue(row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result == true {
		t.Errorf("equalClause.IsTrue(%v) is not supposed to be true", row)
	}
}

func TestEqualClauseIsTrueWithEqual(t *testing.T) {
	equalClause := EqualClause{Left: "first", Right: "second"}
	row := []string{"1", "1"}
	equalClause.Init([]string{"first", "second"})
	result, err := equalClause.IsTrue(row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result == false {
		t.Errorf("equalClause.IsTrue(%v) is supposed to be true", row)
	}
}

func TestEqualClauseIsTrueWithoutInit(t *testing.T) {
	equalClause := EqualClause{Left: "first", Right: "second"}
	row := []string{"0", "1"}
	expected := "EqualClause not initialized"
	_, err := equalClause.IsTrue(row)
	if err == nil {
		t.Error("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("%v should equal %v", err.Error(), expected)
	}
}

func TestEqualClauseInit(t *testing.T) {
	equalClause := EqualClause{Left: "first", Right: "second"}
	expected := EqualClause{"first", "second", true, 0, 1}
	headers := []string{"first", "second"}
	err := equalClause.Init(headers)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if expected != equalClause {
		t.Errorf("%v should equal %v", equalClause, expected)
	}
}

func TestEqualClauseInitInvalid(t *testing.T) {
	equalClause := EqualClause{Left: "first", Right: "notHere"}
	expected := "first, notHere not found in headers"
	headers := []string{"first", "second"}
	err := equalClause.Init(headers)
	if err == nil {
		t.Error("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("%v should equal %v", err.Error(), expected)
	}
}
