// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import (
	"errors"
	"fmt"
	"math/big"
	"strings"
)

// GreaterClause describe a greater clause true when ToBeGreater is greater than ToBeLesser
type GreaterClause struct {
	ToBeGreater      string
	ToBeLesser       string
	Initialized      bool
	ToBeGreaterIndex int
	ToBeLesserIndex  int
}

// Init for GreaterClause initiate index from headers
func (greaterClause *GreaterClause) Init(headers []string) error {
	found := 0
	for index, value := range headers {
		trimedValue := strings.Trim(value, " ")
		if trimedValue == greaterClause.ToBeGreater {
			greaterClause.ToBeGreaterIndex = index
			found++
		} else if trimedValue == greaterClause.ToBeLesser {
			greaterClause.ToBeLesserIndex = index
			found++
		}
		if found == 2 {
			greaterClause.Initialized = true
			break
		}
	}
	if found != 2 {
		return fmt.Errorf("%v, %v not found in headers", greaterClause.ToBeGreater, greaterClause.ToBeLesser)
	}
	return nil
}

// IsTrue for GreaterClause returns true when the value of IndexToBeGreater is greater than the value IndexToBeLesser
func (greaterClause *GreaterClause) IsTrue(row []string) (bool, error) {
	if !greaterClause.Initialized {
		return false, errors.New("GreaterClause not initialized")
	}
	valueToBeGreater := new(big.Float)
	_, _, err1 := valueToBeGreater.Parse(row[greaterClause.ToBeGreaterIndex], 10)
	valueToBeLesser := new(big.Float)
	_, _, err2 := valueToBeLesser.Parse(row[greaterClause.ToBeLesserIndex], 10)

	if err1 == nil && err2 == nil {
		return valueToBeGreater.Cmp(valueToBeLesser) == 1, nil
	}
	return false, fmt.Errorf("error on field1: %v and on field2: %v", err1, err2)
}

// String allow to convert an EqualClause to string
func (greaterClause *GreaterClause) String() string {
	return fmt.Sprintf("ToBeGreater: %v ToBeLesser: %v", greaterClause.ToBeGreater, greaterClause.ToBeLesser)
}
