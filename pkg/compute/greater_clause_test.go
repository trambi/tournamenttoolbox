// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compute

import "testing"

func TestGreaterClauseIsTrueWithLesser(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "first", ToBeLesser: "second"}
	row := []string{"0", "1"}
	greaterClause.Init([]string{"first", "second"})
	result, err := greaterClause.IsTrue(row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result == true {
		t.Errorf("greaterClause.IsTrue(%v) is not supposed to be true", row)
	}
}
func TestGreaterClauseIsTrueWithEqual(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "first", ToBeLesser: "second"}
	row := []string{"0", "0"}
	greaterClause.Init([]string{"first", "second"})
	result, err := greaterClause.IsTrue(row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result == true {
		t.Errorf("greaterClause.IsTrue(%v) is not supposed to be true", row)
	}
}

func TestGreaterClauseIsTrueWithGreater(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "first", ToBeLesser: "second"}
	row := []string{"1", "0"}
	greaterClause.Init([]string{"first", "second"})
	result, err := greaterClause.IsTrue(row)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result == false {
		t.Errorf("greaterClause.IsTrue(%v) is supposed to be true", row)
	}
}

func TestGreaterClauseIsTrueWithoutInit(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "first", ToBeLesser: "second"}
	row := []string{"0", "1"}
	expected := "GreaterClause not initialized"
	_, err := greaterClause.IsTrue(row)
	if err == nil {
		t.Error("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("%v should equal %v", err.Error(), expected)
	}
}

func TestGreaterClauseInit(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "first", ToBeLesser: "second"}
	expected := GreaterClause{"first", "second", true, 0, 1}
	headers := []string{"first", "second"}
	err := greaterClause.Init(headers)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if expected != greaterClause {
		t.Errorf("%v should equal %v", greaterClause, expected)
	}
}

func TestGreaterClauseInitInvalid(t *testing.T) {
	greaterClause := GreaterClause{ToBeGreater: "notHere", ToBeLesser: "second"}
	expected := "notHere, second not found in headers"
	headers := []string{"first", "second"}
	err := greaterClause.Init(headers)
	if err == nil {
		t.Error("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("%v should equal %v", err.Error(), expected)
	}
}
