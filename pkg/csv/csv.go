// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package csv

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

type IToDoubleArrayOfString interface {
	To() [][]string
}

type IFromDoubleArrayOfString interface {
	From(doubleArrayOfString [][]string) error
}

// FromFile imports []pair.Pairing from a csv file
func FromFile(serializer IFromDoubleArrayOfString, csvPath string) error {
	csvFile, err := os.Open(csvPath)
	// if os.Open returns an error then handle it
	if err != nil {
		return fmt.Errorf("unable to read file [%v]", err.Error())
	}
	defer csvFile.Close()
	r := csv.NewReader(csvFile)
	var records [][]string
	records, err = r.ReadAll()
	if err != nil {
		return nil
	}
	err = serializer.From(records)
	return err
}

// ToFile exports Ranking as a csv file
func ToFile(serializer IToDoubleArrayOfString, csvPath string) error {
	csvFile, err := os.Create(csvPath)
	if err == nil {
		writer := csv.NewWriter(csvFile)
		defer writer.Flush()
		err = writer.WriteAll(serializer.To())
		if err != nil {
			err = fmt.Errorf("error in writing in file [%s]: %s", csvPath, err.Error())
			return err
		}
	} else {
		err = fmt.Errorf("unable to write in such path [%s]: %s", csvPath, err.Error())
		return err
	}
	log.Printf("Ranking writed in %s\n", csvPath)
	return nil
}
