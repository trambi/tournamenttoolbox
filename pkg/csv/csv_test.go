// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package csv

import (
	"bufio"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

type TestExporter struct{}

func (_ TestExporter) To() [][]string {
	return [][]string{
		{"Column 1", "Column 2"},
		{"A", "B"},
		{"C", "D"},
	}
}

type TestImporter struct {
	content []string
}

func (ti *TestImporter) From(lines [][]string) error {
	for _, line := range lines {
		ti.content = append(ti.content, strings.Join(line, ","))
	}
	return nil
}

func TestToFile(t *testing.T) {
	outputDir, _ := ioutil.TempDir("", "ranking_")
	outputPath := filepath.Join(outputDir, "ranking_for_test.csv")
	defer os.RemoveAll(outputDir)
	testExport := TestExporter{}
	expected := []string{
		"Column 1,Column 2",
		"A,B",
		"C,D"}

	result := []string{}
	err := ToFile(testExport, outputPath)
	if err != nil {
		t.Errorf("unexpected error during toCsvFile: %v", err.Error())
	}
	f, err := os.Open(outputPath)
	defer f.Close()
	if err != nil {
		t.Errorf("unexpected error in os.Open: %v", err.Error())
	}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		t.Errorf("unexpected error during reading: %v", err.Error())
	}
	if cmp.Equal(result, expected) == false {
		t.Errorf("result %v differs from expected %v", result, expected)
	}
}

func TestToFileWithInvalidPath(t *testing.T) {
	testExport := TestExporter{}
	err := ToFile(testExport, "/invalid/path")
	if err == nil {
		t.Error("Expected error got nil")
	}
	if strings.HasPrefix(err.Error(), "unable to write in such path ") == false {
		t.Errorf("unexpected error during ToFile: %v", err.Error())
	}
}

func TestRowsFromFile(t *testing.T) {
	expected := TestImporter{
		content: []string{
			"a,b,c,d",
			"e,f,g,h",
		},
	}
	testImport := TestImporter{}
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	err := FromFile(&testImport, filepath.Join(relPath, "..", "..", "test", "test_import.csv"))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(testImport.content, expected.content) == false {
		t.Errorf("rankingRow %v are different from expected %v", testImport, expected)
	}
}
