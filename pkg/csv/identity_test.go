// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package csv

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIdentityTo(t *testing.T) {
	input := Identity{
		Content: [][]string{
			{"a", "b"},
			{"c", "d", "e"},
		}}
	expected := [][]string{
		{"a", "b"},
		{"c", "d", "e"},
	}
	result := input.To()
	if cmp.Equal(result, expected) == false {
		t.Errorf("result %v differs from expected %v", result, expected)
	}
}

func TestIdentityFrom(t *testing.T) {
	id := &Identity{}
	input := [][]string{
		{"a", "b"},
		{"c", "d", "e"},
	}
	expected := [][]string{
		{"a", "b"},
		{"c", "d", "e"},
	}
	err := id.From(input)
	if err != nil {
		t.Errorf("unexpected error during From: %v", err.Error())
	}
	if cmp.Equal(id.Content, expected) == false {
		t.Errorf("result %v differs from expected %v", id.Content, expected)
	}
}
