// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package csv

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/trambi/tournamenttoolbox/pkg/pair"
)

type PairingsCsv struct {
	Pairings []pair.Pairing
}

func (csv *PairingsCsv) From(doubleArrayOfString [][]string) error {
	if len(doubleArrayOfString) < 1 {
		return errors.New("Not enough array of strings to create array of RankingRow")
	}
	index1, index2, err := GetIndexesFromHeaders(doubleArrayOfString[0], "id")
	if err != nil {
		return err
	}
	for _, array := range doubleArrayOfString[1:] {
		row, err := fromStringArray(array, index1, index2)
		if err != nil {
			return err
		}
		csv.Pairings = append(csv.Pairings, row)
	}
	return nil
}

func (csv PairingsCsv) To() [][]string {
	result := make([][]string, len(csv.Pairings)+1)
	result[0] = []string{"id_1", "id_2"}
	for i, row := range csv.Pairings {
		result[i+1] = toStringArray(row)
	}
	return result
}

func toStringArray(pairing pair.Pairing) []string {
	return []string{pairing.Opponent1, pairing.Opponent2}
}

func GetIndexesFromHeaders(headers []string, prefixId string) (int, int, error) {
	index1, index2 := -1, -1
	field1 := prefixId + "_1"
	field2 := prefixId + "_2"
	for index, header := range headers {
		trimedHeader := strings.Trim(header, " ")
		if trimedHeader == field1 {
			index1 = index
		} else if trimedHeader == field2 {
			index2 = index
		}
	}
	if index1 == -1 && index2 == -1 {
		return -1, -1, fmt.Errorf("%s and %s columns not found", field1, field2)
	} else if index1 == -1 {
		return -1, index2, fmt.Errorf("%s column not find", field1)
	} else if index2 == -1 {
		return index1, -1, fmt.Errorf("%s column not find", field2)
	}
	return index1, index2, nil
}

func fromStringArray(array []string, index1 int, index2 int) (pair.Pairing, error) {
	if len(array) < 2 {
		return pair.Pairing{},
			fmt.Errorf("not enough strings to create Pairing from array: %v", array)
	}
	if len(array) < index1+1 || index1 < 0 {
		return pair.Pairing{}, errors.New("Out of array index 1")
	}
	if len(array) < index2+1 || index2 < 0 {
		return pair.Pairing{}, errors.New("Out of array index 2")
	}
	return pair.Pairing{
		Opponent1: array[index1],
		Opponent2: array[index2]}, nil
}
