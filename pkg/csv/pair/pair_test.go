// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package csv

import (
	"fmt"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/pair"
)

func TestGetIndexesFromHeaders(t *testing.T) {
	header := []string{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	index1, index2, err := GetIndexesFromHeaders(header, "id")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if index1 != 0 || index2 != 4 {
		t.Errorf("index1 %v should equal 0 and index2 %v should equal 4", index1, index2)
	}
}

func TestPairingFromStringArray(t *testing.T) {
	expected := pair.Pairing{Opponent1: "1", Opponent2: "2"}
	row := []string{"1", "useless_field", "another_useless_field", "2"}
	type TestStruct struct {
		input1         []string
		input2         int
		input3         int
		expectedResult pair.Pairing
		expectedError  string
	}
	outOfArray := "Out of array index "
	tests := []TestStruct{
		{input1: row, input2: 0, input3: 3, expectedResult: expected, expectedError: ""},
		{input1: row, input2: 4, input3: 3, expectedResult: pair.Pairing{}, expectedError: outOfArray},
		{input1: row, input2: -1, input3: 3, expectedResult: pair.Pairing{}, expectedError: outOfArray},
		{input1: row, input2: 0, input3: 4, expectedResult: pair.Pairing{}, expectedError: outOfArray},
		{input1: row, input2: 0, input3: -1, expectedResult: pair.Pairing{}, expectedError: outOfArray},
	}
	for _, test := range tests {
		call := fmt.Sprintf("PairingFromStringArray(%v, %d, %d)",
			test.input1, test.input2, test.input3)
		result, err := fromStringArray(test.input1, test.input2, test.input3)
		if test.expectedError == "" {
			if err != nil {
				t.Errorf("%v returns an unexpected error: %v",
					call, err.Error())
			}
			if cmp.Equal(result, expected) == false {
				t.Errorf("result %v differs from expected %v", result, expected)
			}
		} else {
			if err == nil {
				t.Errorf("%v should return an error",
					call)
			} else if strings.HasPrefix(err.Error(), test.expectedError) == false {

			}
		}
	}
}

func TestFrom(t *testing.T) {
	table := [][]string{
		{"id_1", "useless_field", "another_useless_field", "id_2"},
		{"1", "useless", "useless", "2"},
		{"3", "useless", "useless", "4"},
	}
	expected := []pair.Pairing{
		{Opponent1: "1", Opponent2: "2"},
		{Opponent1: "3", Opponent2: "4"},
	}
	csv := &PairingsCsv{}
	err := csv.From(table)
	if err != nil {
		t.Errorf("From(%v) returns an unexpected error: %v",
			table, err.Error())
	}
	if cmp.Equal(csv.Pairings, expected) == false {
		t.Errorf("From should return %v instead of %v", expected, csv.Pairings)
	}
	csv = &PairingsCsv{}
	table = [][]string{}
	err = csv.From([][]string{})
	if err == nil {
		t.Errorf("From(%v) should return an error", table)
	}
	if len(csv.Pairings) != 0 {
		t.Errorf("From should return empty instead of %v", csv.Pairings)
	}
	table = [][]string{{"no_id_1", "no_id_2"}}
	err = csv.From(table)
	if err == nil {
		t.Errorf("From(%v) should return an error", table)
	}
	expectedError := "id_1 and id_2 columns not found"
	if err.Error() != expectedError {
		t.Errorf("From(%v) should return an error: %v instead of %v", table, expectedError, err.Error())
	}
}
