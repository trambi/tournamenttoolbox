// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package csv

import (
	"fmt"

	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
)

type RankingCsv struct {
	R rank.Ranking
}

type RankingRowsCsv struct {
	Rows []rank.RankingRow
}

func (csv RankingCsv) To() [][]string {
	result := make([][]string, len(csv.R.Rows)+1)
	result[0] = make([]string, len(csv.R.Settings.Criterias)+2)
	result[0][0] = csv.R.Settings.IDColumnName
	result[0][1] = csv.R.Settings.NameColumnName
	for i, criteria := range csv.R.Settings.Criterias {
		result[0][i+2] = criteria.Name
	}
	for i, row := range csv.R.Rows {
		result[i+1] = toStringArray(row)
	}
	return result
}

func toStringArray(rr rank.RankingRow) []string {
	return append([]string{rr.ID, rr.Name}, rr.Values...)
}

func rowFromStringArray(array []string) (rank.RankingRow, error) {
	if len(array) < 2 {
		return rank.RankingRow{},
			fmt.Errorf("not enough strings to create RankingRow from array: %v", array)
	}
	return rank.RankingRow{Name: array[1], ID: array[0], Values: array[2:]}, nil
}

func (csv *RankingRowsCsv) From(doubleArrayOfString [][]string) error {
	if len(doubleArrayOfString) < 2 {
		return fmt.Errorf("not enough array of strings to create array of RankingRow")
	}
	for _, array := range doubleArrayOfString[1:] {
		row, err := rowFromStringArray(array)
		if err != nil {
			return err
		}
		csv.Rows = append(csv.Rows, row)
	}
	return nil
}
