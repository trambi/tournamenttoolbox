// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package csv

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
)

func rankingForTest() rank.Ranking {
	return rank.Ranking{Settings: settingsForTest(), Rows: []rank.RankingRow{}}
}

func settingsForTest() rank.Settings {
	pointsColumn := rank.Criteria{
		Name: "Points", FieldPrefix: "points",
		GameScoped: true, OpponentScoped: false}
	tdColumn := rank.Criteria{
		Name: "TD", FieldPrefix: "td",
		GameScoped: true, OpponentScoped: false}
	settings := rank.Settings{
		IDColumnName:   "ID",
		NameColumnName: "Name",
		Criterias:      []rank.Criteria{pointsColumn, tdColumn},
	}
	settings.Init([]string{"id_1", "team_1", "points_1", "td_1", "id_2",
		"team_2", "points_2", "td_2"})
	return settings
}

func TestToStringArray(t *testing.T) {
	row := rank.RankingRow{
		Name: "first", ID: "id1", Values: []string{"3", "1"}}
	expected := []string{"id1", "first", "3", "1"}
	result := toStringArray(row)
	if cmp.Equal(result, expected) == false {
		t.Errorf("result %v differs from expected %v", result, expected)
	}
}

func TestToDoubleArrayOfString(t *testing.T) {
	ranking := rankingForTest()
	ranking.Rows = []rank.RankingRow{
		{Name: "first", ID: "id1", Values: []string{"3", "1"}},
		{Name: "second", ID: "id2", Values: []string{"3", "0"}},
	}
	csv := RankingCsv{R: ranking}
	expected := [][]string{
		{"ID", "Name", "Points", "TD"},
		{"id1", "first", "3", "1"},
		{"id2", "second", "3", "0"},
	}
	result := csv.To()
	if cmp.Equal(result, expected) == false {
		t.Errorf("result %v differs from expected %v", result, expected)
	}
}

func TestRowFromStringArray(t *testing.T) {
	expected := rank.RankingRow{
		Name:   "first_team",
		ID:     "1",
		Values: []string{"3", "2", "3"},
	}
	result, err := rowFromStringArray([]string{"1", "first_team", "3", "2", "3"})
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(result, expected) == false {
		t.Errorf("rankingRow %v are different from expected %v", result, expected)
	}
	_, err = rowFromStringArray([]string{"1"})
	if err == nil {
		t.Errorf("Expected error got nil")
	}
	expectedError := "not enough strings"
	if strings.HasPrefix(err.Error(), expectedError) == false {
		t.Errorf("unexpected error during rowFromStringArray: %v (should begin with %v)", err.Error(), expectedError)
	}
}

func TestRowsFromDoubleArrayOfString(t *testing.T) {
	expected := []rank.RankingRow{
		{
			Name:   "first_team",
			ID:     "1",
			Values: []string{"3", "2", "3"},
		},
		{
			Name:   "second_team",
			ID:     "2",
			Values: []string{"3", "2", "2"},
		},
	}
	input := [][]string{
		{"id", "name", "points", "td_net", "td"},
		{"1", "first_team", "3", "2", "3"},
		{"2", "second_team", "3", "2", "2"},
	}
	var csv RankingRowsCsv
	err := csv.From(input)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if cmp.Equal(csv.Rows, expected) == false {
		t.Errorf("rankingRow[] %v are different from expected %v", csv.Rows, expected)
	}
	err = csv.From([][]string{})
	if err == nil {
		t.Errorf("Expected error got nil")
	}
	expectedError := "not enough array of strings"
	if strings.HasPrefix(err.Error(), expectedError) == false {
		t.Errorf("unexpected error during rowsFromDoubleArrayOfString: %v (should begin with %v)", err.Error(), expectedError)
	}
	err = csv.From([][]string{{"id", "name"}, {}})
	if err == nil {
		t.Errorf("Expected error got nil")
	}
	expectedError = "not enough strings"
	if strings.HasPrefix(err.Error(), expectedError) == false {
		t.Errorf("unexpected error during rowsFromDoubleArrayOfString: %v (should begin with %v)", err.Error(), expectedError)
	}
}
