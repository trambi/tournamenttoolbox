// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com/
package evaluator

import (
	"fmt"
)

type Context map[string]string

// Evaluate take input parse it and return the evaluation of this
func Evaluate(input string, context Context) (string, error) {
	parser, err := NewParser(input)
	if err != nil {
		return "", fmt.Errorf("unable to initiate parser: %v", err.Error())
	}
	ast, err := parser.expr()
	if err != nil {
		return "", fmt.Errorf("unable to parse the expression: %v", err.Error())
	}
	return visit(ast, context)
}

func visit(node INode, context Context) (string, error) {
	return node.getValue(context)
}
