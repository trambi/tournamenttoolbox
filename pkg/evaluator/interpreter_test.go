// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com
package evaluator

import (
	"strings"
	"testing"
)

func TestEvaluateEmptyString(t *testing.T) {
	_, err := Evaluate("", make(Context))
	expected := "unable to initiate parser: "
	if err == nil {
		t.Error("Should return an error")
	} else if strings.HasPrefix(err.Error(), expected) == false {
		t.Errorf("Unexpected error :%v should begin with: %v", err.Error(), expected)
	}
}
func TestEvaluteWithAnInteger(t *testing.T) {
	result, err := Evaluate("1", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "1" {
		t.Errorf("result %v is different from expected 1", result)
	}
}

func TestEvaluteWithAnDecimal(t *testing.T) {
	expected := "1.5"
	result, err := Evaluate("1.5", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != expected {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestEvaluteWithAdditionTwoIntegers(t *testing.T) {
	result, err := Evaluate("1+2", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "3" {
		t.Errorf("result %v is different from expected 3", result)
	}
}
func TestEvaluteWithAdditionAnIntegerAndAnNumber(t *testing.T) {
	result, err := Evaluate("1+2.5", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "3.5" {
		t.Errorf("result %v is different from expected 3.5", result)
	}
}

func TestEvaluteWithAdditionAnNumberAndAnInteger(t *testing.T) {
	result, err := Evaluate("1.5+2", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "3.5" {
		t.Errorf("result %v is different from expected 3.5", result)
	}
}

func TestEvaluteWithAdditionTwoNumbers(t *testing.T) {
	result, err := Evaluate("1.25+2.25", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "3.5" {
		t.Errorf("result %v is different from expected 3.5", result)
	}
}

func TestEvaluteWithSubstractionTwoIntegers(t *testing.T) {
	result, err := Evaluate("3-2", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "1" {
		t.Errorf("result %v is different from expected 1", result)
	}
}

func TestEvaluteWithSubstractionTwoNumbers(t *testing.T) {
	result, err := Evaluate("3.3-2.25", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "1.05" {
		t.Errorf("result %v is different from expected 1.05", result)
	}
}

func TestEvaluteUnknownSymbol(t *testing.T) {
	_, err := Evaluate("(", make(Context))
	expected := "unable to parse the expression: unexpected token (EOF)"
	if err == nil {
		t.Errorf("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("error %v is different from expected (%v)", err.Error(), expected)
	}
}

func TestEvaluteOnlyPlus(t *testing.T) {
	_, err := Evaluate("+", make(Context))
	expected := "unable to parse the expression: unexpected token (PLUS)"
	if err == nil {
		t.Errorf("Should return an error\n")
	} else if err.Error() != expected {
		t.Errorf("error \"%v\" is different from expected \"%v\"", err.Error(), expected)
	}
}

func TestEvaluteWithAdditionThreeNumbers(t *testing.T) {
	result, err := Evaluate("1+2+3", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "6" {
		t.Errorf("result %v is different from expected 6", result)
	}
}

func TestEvaluteWithAdditionAndMultiplication(t *testing.T) {
	result, err := Evaluate("1+2*3", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "7" {
		t.Errorf("result %v is different from expected 7", result)
	}
	result, err = Evaluate("1*2+3*4", make(Context))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "14" {
		t.Errorf("result %v is different from expected 14", result)
	}
}

func TestEvaluateToIncrementOneVariable(t *testing.T) {
	context := make(Context)
	context["td_1"] = "2"
	result, err := Evaluate("{td_1}+1", context)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "3" {
		t.Errorf("result %v is different from expected 3", result)
	}
}

func TestEvaluateAddTwoVariables(t *testing.T) {
	context := make(Context)
	context["td_1"] = "2"
	context["td_2"] = "1"
	result, err := Evaluate("{td_1}-{td_2}", context)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if result != "1" {
		t.Errorf("result %v is different from expected 1", result)
	}
}

func TestEvaluateUnknownVariable(t *testing.T) {
	context := make(Context)
	context["td_1"] = "2"
	result, err := Evaluate("{unknown}", context)
	expected := "unknown variable \"unknown\""
	if err == nil {
		t.Errorf("Should return an error and returns %v\n", result)
	} else if err.Error() != expected {
		t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
	}
}

func TestEvaluateLeftUnknownVariable(t *testing.T) {
	context := make(Context)
	context["td_1"] = "2"
	expected := "error in evaluating node value: unknown variable \"unknown\""
	result, err := Evaluate("{unknown}+1", context)
	if err == nil {
		t.Errorf("Should return an error and returns %v\n", result)
	} else if err.Error() != expected {
		t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
	}
}

func TestEvaluateRightUnknownVariable(t *testing.T) {
	context := make(Context)
	context["td_1"] = "2"
	expected := "error in evaluating node value: unknown variable \"unknown\""
	result, err := Evaluate("1+{unknown}", context)
	if err == nil {
		t.Errorf("Should return an error and returns %v\n", result)
	} else if err.Error() != expected {
		t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
	}
}
