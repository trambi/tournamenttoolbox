// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com/
package evaluator

import (
	"errors"
	"fmt"
	"math/big"
)

// Parser is in charge of interpreting the tokens furnished by tokenizer
type Parser struct {
	tokenizer    *Tokenizer
	CurrentToken Token
}

// INode is a interface the abstract node of Abstract Symbolic Tree
type INode interface {
	String() string
	getValue(context Context) (string, error)
}

// BinOpNode is binary operator node,
// it has left node - first node of the binary operator node
// it has right node - second node of the binary operator node
type BinOpNode struct {
	token Token
	left  INode
	right INode
}

// String return a string for the binary operator node -- useful to debug and test
func (node BinOpNode) String() string {
	return fmt.Sprintf("[%v] <--[%v]--> [%v]", node.left, node.token.value, node.right)
}

func extractFloatFromINode(node INode, context Context) (*big.Float, error) {
	value, err := node.getValue(context)
	if err != nil {
		return new(big.Float), fmt.Errorf("error in evaluating node value: %v", err.Error())
	}
	valueInFloat := new(big.Float)
	_, _, err = valueInFloat.Parse(value, 10)
	if err != nil {
		return new(big.Float), fmt.Errorf("error in converting node value to integer: %v", err.Error())
	}
	return valueInFloat, nil
}

// getValue return the value of the binary operator apply to left node and right node
func (node BinOpNode) getValue(context Context) (string, error) {
	left, err := extractFloatFromINode(node.left, context)
	if err != nil {
		return "", err
	}
	right, err := extractFloatFromINode(node.right, context)
	if err != nil {
		return "", err
	}
	result := new(big.Float)
	switch node.token.tokenType {
	case PLUS:
		return result.Add(left, right).String(), nil
	case MINUS:
		return result.Sub(left, right).String(), nil
	case MUL:
		return result.Mul(left, right).String(), nil
	default:
		return "", fmt.Errorf("unknown operator type %v", node.token.tokenType)
	}
}

// NumNode is number node
type NumNode struct {
	token Token
	value string
}

// String return a string for the number node -- useful to debug and test
func (node NumNode) String() string {
	return fmt.Sprintf("%v", node.token.value)
}

// getValue return value converted into string of number node
func (node NumNode) getValue(context Context) (string, error) {
	return node.value, nil
}

// VarNode is variable node
type VarNode struct {
	token Token
	value string
}

// String return a string for the variable node -- useful to debug and test
func (node VarNode) String() string {
	return fmt.Sprintf("{%v}", node.token.value)
}

// getValue return value according to context converted into string of variable node
func (node VarNode) getValue(context Context) (string, error) {
	value, exist := context[node.value]
	if !exist {
		return "", fmt.Errorf("unknown variable \"%v\"", node.value)
	}
	return value, nil
}

// ErrorNode is a node for error case
type ErrorNode struct{}

// String return a string for the error node -- useful to debug and test
func (node ErrorNode) String() string {
	return "Error !"
}

// getValue return value of error node always an error
func (node ErrorNode) getValue(context Context) (string, error) {
	return "Error !", fmt.Errorf("try to value of an ErrorNode")
}

// NewParser create an parser from an input string
func NewParser(input string) (Parser, error) {
	tokenizer, err := NewTokenizer(input)
	if err == nil {
		return Parser{tokenizer: &tokenizer}, nil
	}
	return Parser{}, errors.New("unable to create an tokenizer for the parser")
}

func (parser *Parser) eat(tokenType TokenType) error {
	if parser.CurrentToken.tokenType == tokenType {
		var err error
		parser.CurrentToken, err = parser.tokenizer.nextToken()
		return err
	}
	return fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType)

}

// term return an NUMBER token value
func (parser *Parser) term() (INode, error) {
	// term : factor (MUL factor)*
	node, err := parser.factor()
	if err != nil {
		return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
	}
	for parser.CurrentToken.tokenType == MUL {
		op := parser.CurrentToken
		if op.tokenType == MUL {
			parser.eat(MUL)
			var factor INode
			factor, err = parser.factor()
			if err != nil {
				return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			}
			node = BinOpNode{op, node, factor}
		} else {
			return ErrorNode{}, fmt.Errorf("invalid operator! - Token type: %v", op.tokenType.String())
		}
	}
	return node, nil
}

// factor returns an NUMBER token or expr token
func (parser *Parser) factor() (INode, error) {
	// factor: NUMBER | LPAREN expr RPAREN | LCBRACKET VARIABLE RCBRACKET
	token := parser.CurrentToken
	if token.tokenType == NUMBER {
		err := parser.eat(NUMBER)
		if err != nil {
			return ErrorNode{}, err
		}
		return NumNode{token, token.value}, err
	} else if token.tokenType == LPAREN {
		err := parser.eat(LPAREN)
		if err != nil {
			return ErrorNode{}, err
		}
		node, err := parser.expr()
		if err != nil {
			return ErrorNode{}, err
		}
		err = parser.eat(RPAREN)
		if err != nil {
			return ErrorNode{}, err
		}
		return node, nil
	} else if token.tokenType == LCBRACKET {
		err := parser.eat(LCBRACKET)
		if err != nil {
			return ErrorNode{}, err
		}
		token = parser.CurrentToken
		value := token.value
		err = parser.eat(VARIABLE)
		if err != nil {
			return ErrorNode{}, err
		}
		err = parser.eat(RCBRACKET)
		if err != nil {
			return ErrorNode{}, err
		}
		return VarNode{token, value}, nil
	}
	return ErrorNode{}, fmt.Errorf("unknown token for a term: %v", token)
}

func (parser *Parser) expr() (INode, error) {
	// expr: term ((PLUS|MINUS) term)*
	// term: factor(MUL factor)*
	// factor: NUMBER | LPAREN expr RPAREN
	var err error
	parser.CurrentToken, err = parser.tokenizer.nextToken()
	if err != nil {
		return ErrorNode{}, err
	}
	node, err := parser.term()
	if err != nil {
		return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
	}
	for parser.CurrentToken.tokenType == PLUS || parser.CurrentToken.tokenType == MINUS {
		op := parser.CurrentToken
		if op.tokenType == PLUS {
			err = parser.eat(PLUS)
			if err != nil {
				return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			}
		} else if op.tokenType == MINUS {
			err = parser.eat(MINUS)
			if err != nil {
				return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
			}
		} else {
			return ErrorNode{}, fmt.Errorf("invalid operator! - Token type: %v", op.tokenType.String())
		}
		var term INode
		term, err = parser.term()
		if err != nil {
			return ErrorNode{}, fmt.Errorf("unexpected token (%v)", parser.CurrentToken.tokenType.String())
		}
		node = BinOpNode{op, node, term}
	}
	return node, nil
}
