// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com
package evaluator

import (
	"testing"
)

func TestNewParser(t *testing.T) {
	expectedTokenizer := Tokenizer{"1", 0, Token{EOF, "EOF"}, '1'}
	parser, err := NewParser("1")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if *parser.tokenizer != expectedTokenizer {
		t.Errorf("parser %v is different from expected %v", parser.tokenizer, expectedTokenizer)
	}
}

func TestNewParserWithEmptyInput(t *testing.T) {
	parser, err := NewParser("")
	expected := Parser{}
	if err == nil {
		t.Errorf("Error was expected\n")
	}
	if parser != expected {
		t.Errorf("parser %v is supposed to be empty", parser)
	}
}

func TestEatInteger(t *testing.T) {
	parser, err := NewParser("1")
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	_, _ = parser.tokenizer.nextToken()
	err = parser.eat(NUMBER)
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
}

func TestEatIntegerInvalid(t *testing.T) {
	parser, err := NewParser("1")
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	_, _ = parser.tokenizer.nextToken()
	err = parser.eat(EOF)
	if err == nil {
		t.Errorf("Should an error\n")
	}
}

func TestExprNumberPlusNumber(t *testing.T) {
	parser, err := NewParser("1+2")
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	var node INode
	node, err = parser.expr()
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	if node.String() != "[1] <--[+]--> [2]" {
		t.Errorf("Unexpected returned node: %v", node)
	}
}

func TestExprVariable(t *testing.T) {
	parser, err := NewParser("{var_1}")
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	var node INode
	node, err = parser.expr()
	if err != nil {
		t.Errorf("Unexpected error %v\n", err.Error())
	}
	if node.String() != "{var_1}" {
		t.Errorf("Unexpected returned node: %v", node)
	}
}

func TestBinOpNodeGetValueWithInvalidLeftNode(t *testing.T) {
	node := BinOpNode{Token{PLUS, "+"}, ErrorNode{}, NumNode{Token{NUMBER, "12"}, "12"}}
	_, err := node.getValue(Context{})
	if err == nil {
		t.Error("Should return an error")
	}
}

func TestBinOpNodeGetValueWithInvalidRightNode(t *testing.T) {
	node := BinOpNode{Token{PLUS, "+"}, NumNode{Token{NUMBER, "12"}, "12"}, ErrorNode{}}
	_, err := node.getValue(Context{})
	if err == nil {
		t.Error("Should return an error")
	}
}

func TestBinOpNodeGetValueWithInvalidType(t *testing.T) {
	node := BinOpNode{Token{NUMBER, "0"}, NumNode{Token{NUMBER, "1"}, "1"}, NumNode{Token{NUMBER, "2"}, "2"}}
	_, err := node.getValue(Context{})
	if err == nil {
		t.Error("Should return an error")
	}
}
