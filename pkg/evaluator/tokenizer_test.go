// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com
package evaluator

import (
	"testing"
)

func TestNewTokenizer(t *testing.T) {
	expected := Tokenizer{"1", 0, Token{EOF, "EOF"}, '1'}
	result, err := NewTokenizer("1")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if result != expected {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestNewTokenizerWithEmptyInput(t *testing.T) {
	tokenizer, err := NewTokenizer("")
	expected := Tokenizer{}
	if err == nil {
		t.Errorf("Error was expected\n")
	}
	if tokenizer != expected {
		t.Errorf("tokenizer %v is supposed to be empty", tokenizer)
	}
}

func TestAdvance(t *testing.T) {
	tokenizer, err := NewTokenizer("12")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	tokenizer.advance()
	if tokenizer.Pos != 1 || tokenizer.CurrentByte != '2' {
		t.Errorf("Pos is %v and CurrentByte %v \n", tokenizer.Pos, tokenizer.CurrentByte)
	}
	tokenizer.advance()
	if tokenizer.Pos != 2 || tokenizer.CurrentByte != 0 {
		t.Errorf("Pos is %v and CurrentByte %v \n", tokenizer.Pos, tokenizer.CurrentByte)
	}
}

func TestSkipWhiteSpace(t *testing.T) {
	tokenizer, err := NewTokenizer(" 12")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	tokenizer.skipWhiteSpace()
	if tokenizer.Pos != 1 || tokenizer.CurrentByte != '1' {
		t.Errorf("Pos is %v and CurrentByte %v \n", tokenizer.Pos, tokenizer.CurrentByte)
	}
}

func TestInteger(t *testing.T) {
	tokenizer, err := NewTokenizer("12")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	value := tokenizer.decimal()
	if value != "12" {
		t.Errorf("Unexpected value: %v\n", value)
	}
}

func TestGetNextTokenInteger(t *testing.T) {
	tokenizer, err := NewTokenizer(" 1+23 - 456 ")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != NUMBER || token.value != "1" {
		t.Errorf("token.tokenType %v != INTEGER || token.value %v != 1", token.tokenType.String(), token.value)
	}
}
func TestGetNextTokenPlus(t *testing.T) {
	tokenizer, err := NewTokenizer(" +")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != PLUS || token.value != "+" {
		t.Errorf("token.tokenType %v != PLUS || token.value %v != +", token.tokenType.String(), token.value)
	}
}

func TestGetNextTokenMinus(t *testing.T) {
	tokenizer, err := NewTokenizer("- ")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != MINUS || token.value != "-" {
		t.Errorf("token.tokenType %v != MINUS || token.value %v != -", token.tokenType.String(), token.value)
	}
}

func TestGetNextTokenMul(t *testing.T) {
	tokenizer, err := NewTokenizer("* ")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != MUL || token.value != "*" {
		t.Errorf("token.tokenType %v != MUL || token.value %v != *", token.tokenType.String(), token.value)
	}
}

func TestGetNextTokenParenthesis(t *testing.T) {
	tokenizer, err := NewTokenizer("()")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != LPAREN || token.value != "(" {
		t.Errorf("token.tokenType %v != LPAREN || token.value %v != (", token.tokenType.String(), token.value)
	}
	token, err = tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != RPAREN || token.value != ")" {
		t.Errorf("token.tokenType %v != RPAREN || token.value %v != )", token.tokenType.String(), token.value)
	}
}

func TestNextTokenEof(t *testing.T) {
	tokenizer, err := NewTokenizer("12 ")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	_, _ = tokenizer.nextToken()
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != EOF || token.value != "EOF" {
		t.Errorf("token.tokenType %v != EOF || token.value %v != EOF", token.tokenType.String(), token.value)
	}
}

func TestGetNextTokenVariable(t *testing.T) {
	tokenizer, err := NewTokenizer("{var}")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	token, err := tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != LCBRACKET || token.value != "{" {
		t.Errorf("token.tokenType %v != LCBRACKET || token.value %v != {", token.tokenType.String(), token.value)
	}
	token, err = tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != VARIABLE || token.value != "var" {
		t.Errorf("token.tokenType %v != VARIABLE || token.value %v != var", token.tokenType.String(), token.value)
	}
	token, err = tokenizer.nextToken()
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if token.tokenType != RCBRACKET || token.value != "}" {
		t.Errorf("token.tokenType %v != RCBRACKET || token.value %v != }", token.tokenType.String(), token.value)
	}
}

func TestDecimal(t *testing.T) {
	tokenizer, err := NewTokenizer("12.3")
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	value := tokenizer.decimal()
	if value != "12.3" {
		t.Errorf("Unexpected value: %v\n", value)
	}
}
