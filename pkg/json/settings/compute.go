// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"regexp"

	"gitlab.com/trambi/tournamenttoolbox/pkg/compute"
)

type ComputedFieldAdapter struct {
	Name         string             `json:"name"`
	DefaultValue string             `json:"default_value"`
	Statements   []StatementAdapter `json:"statements,omitempty"`
}

type StatementAdapter struct {
	Clause      string `json:"clause"`
	ValueIfTrue string `json:"value_if_true"`
}

func computedFieldFromAdapter(adapter ComputedFieldAdapter) compute.ComputedField {
	computedField := compute.ComputedField{Name: adapter.Name, DefaultValue: adapter.DefaultValue}
	computedField.Statements = make([]compute.Statement, len(adapter.Statements))
	for key, statementAdapter := range adapter.Statements {
		computedField.Statements[key] = statementFromAdapter(statementAdapter)
	}
	return computedField
}

func statementFromAdapter(adapter StatementAdapter) compute.Statement {
	statement := compute.Statement{ValueIfTrue: adapter.ValueIfTrue}
	equalRe := regexp.MustCompile(`{([a-zA-Z_0-9]+)}\s*=\s*{([a-zA-Z_0-9]+)}`)
	equalResult := equalRe.FindAllStringSubmatch(adapter.Clause, 3)
	if len(equalResult) == 1 {
		equalClause := compute.EqualClause{Left: equalResult[0][1], Right: equalResult[0][2]}
		statement.Clause = &equalClause
	} else {
		greaterRe := regexp.MustCompile(`{([a-zA-Z_0-9]+)}\s*>\s*{([a-zA-Z_0-9]+)}`)
		greaterResult := greaterRe.FindAllStringSubmatch(adapter.Clause, 3)
		if len(greaterResult) == 1 {
			greaterClause := compute.GreaterClause{ToBeGreater: greaterResult[0][1], ToBeLesser: greaterResult[0][2]}
			statement.Clause = &greaterClause
		}
	}
	return statement
}
