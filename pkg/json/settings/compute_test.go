// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/compute"
)

func TestStatementFromAdapterWithEqualClause(t *testing.T) {
	expectedClause := compute.EqualClause{Left: "td_1", Right: "td_2"}
	expected := compute.Statement{ValueIfTrue: "1", Clause: &expectedClause}
	adapter := StatementAdapter{Clause: "{td_1} = {td_2}", ValueIfTrue: "1"}
	result := statementFromAdapter(adapter)
	if !cmp.Equal(result, expected) {
		t.Errorf("Statement %v is different from Statement with EqualClause %v", result, expected)
	}
}

func TestStatementFromAdapterWithGreaterClause(t *testing.T) {
	adapter := StatementAdapter{Clause: "{td_1} > {td_2}", ValueIfTrue: "1"}
	expectedClause := compute.GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	expected := compute.Statement{ValueIfTrue: "1", Clause: &expectedClause}
	result := statementFromAdapter(adapter)
	if !cmp.Equal(result, expected) {
		t.Errorf("Statement %v is different from Statement with GreaterClause %v", result, expected)
	}
}
