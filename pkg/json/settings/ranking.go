// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"strings"

	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
)

// CriteriaAdapter is the json adapter for rank.Criteria
type CriteriaAdapter struct {
	Name         string `json:"name"`
	FieldPrefix  string `json:"field_prefix"`
	CriteriaType string `json:"type"`
	Order        string `json:"order"`
}

// RankSettingsAdapter  is the json adapter for rank.Settings
type RankSettingsAdapter struct {
	IDColumnName     string            `json:"competitor_id_column"`
	IDColumnPrefix   string            `json:"competitor_id_field_prefix"`
	NameColumnName   string            `json:"competitor_name_column"`
	NameColumnPrefix string            `json:"competitor_name_field_prefix"`
	Criterias        []CriteriaAdapter `json:"criterias"`
}

func rankSettingsFromAdapter(adapter RankSettingsAdapter) rank.Settings {
	rankSettings := rank.Settings{
		IDColumnPrefix:   strings.Trim(adapter.IDColumnPrefix, " "),
		NameColumnPrefix: strings.Trim(adapter.NameColumnPrefix, " ")}
	if len(adapter.IDColumnName) != 0 {
		rankSettings.IDColumnName = strings.Trim(adapter.IDColumnName, " ")
	} else {
		rankSettings.IDColumnName = "ID"
	}
	if len(adapter.NameColumnName) != 0 {
		rankSettings.NameColumnName = strings.Trim(adapter.NameColumnName, " ")
	} else {
		rankSettings.NameColumnName = "Name"
	}
	for _, criteriaAdapter := range adapter.Criterias {
		criteria := criteriaFromAdapter(criteriaAdapter)
		rankSettings.Criterias = append(rankSettings.Criterias, criteria)
	}

	return rankSettings
}

func criteriaFromAdapter(adapter CriteriaAdapter) rank.Criteria {
	criteria := rank.Criteria{
		Name:        strings.Trim(adapter.Name, " "),
		FieldPrefix: strings.Trim(adapter.FieldPrefix, " ")}
	if adapter.CriteriaType == "sum_per_opponent" {
		criteria.GameScoped = false
		criteria.OpponentScoped = true
		criteria.Accumulator = rank.Sum
	} else {
		criteria.GameScoped = true
		criteria.OpponentScoped = false
		criteria.Accumulator = rank.Sum
	}
	if strings.Trim(adapter.Order, " ") == "ascending" {
		criteria.Less = rank.ReverseLessAsDecimal
	} else {
		criteria.Less = rank.LessAsDecimal
	}
	return criteria
}
