// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
)

func TestCriteriaFromAdapter(t *testing.T) {
	adapter := CriteriaAdapter{"name", "fieldPrefix", "sum_per_game", "descending"}
	expected := rank.Criteria{Name: "name", FieldPrefix: "fieldPrefix",
		GameScoped: true, OpponentScoped: false,
		Accumulator: rank.Sum, Less: rank.LessAsDecimal}
	criteria := criteriaFromAdapter(adapter)
	if expected.Name != criteria.Name {
		t.Errorf("criteria %v is not equal to expected %v by Name", criteria, expected)
	}
	if expected.FieldPrefix != criteria.FieldPrefix {
		t.Errorf("criteria %v is not equal to expected %v by FieldPrefix", criteria, expected)
	}
	if expected.GameScoped != criteria.GameScoped {
		t.Errorf("criteria %v is not equal to expected %v by GameScoped", criteria, expected)
	}
	if expected.OpponentScoped != criteria.OpponentScoped {
		t.Errorf("criteria %v is not equal to expected %v by OpponentScoped", criteria, expected)
	}
}

func TestUnmarshalJSONCriteria(t *testing.T) {
	var criteria rank.Criteria
	var adapter = CriteriaAdapter{}
	expected := rank.Criteria{
		Name: "name", FieldPrefix: "fieldPrefix",
		GameScoped: true, OpponentScoped: false,
		Accumulator: rank.Sum, Less: rank.LessAsDecimal}
	valid := []byte(`{"name":"name","field_prefix":"fieldPrefix","order":"descending","type":"sum_per_game"}`)
	err := json.Unmarshal(valid, &adapter)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	criteria = criteriaFromAdapter(adapter)
	if cmp.Equal(expected, criteria) == false {
		t.Errorf("criteria %v is not equal to expected %v", criteria, expected)
	}
}

func TestRankSettingsFromAdapter(t *testing.T) {
	adapter := RankSettingsAdapter{"IDColumnName", "IDColumnPrefix", "NameColumnName", "NameColumnPrefix", []CriteriaAdapter{}}
	expected := rank.Settings{
		IDColumnName:     "IDColumnName",
		IDColumnPrefix:   "IDColumnPrefix",
		NameColumnName:   "NameColumnName",
		NameColumnPrefix: "NameColumnPrefix"}
	settings := rankSettingsFromAdapter(adapter)
	if cmp.Equal(expected, settings) == false {
		t.Errorf("settings %v is not equal to expected %v", settings, expected)
	}
	adapter = RankSettingsAdapter{
		IDColumnPrefix:   "IDColumnPrefix",
		NameColumnName:   "NameColumnName",
		NameColumnPrefix: "NameColumnPrefix"}
	expected.IDColumnName = "ID"
	settings = rankSettingsFromAdapter(adapter)
	if cmp.Equal(expected, settings) == false {
		t.Errorf("settings %v is not equal to expected %v", settings, expected)
	}
}
