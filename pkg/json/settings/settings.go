// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/trambi/tournamenttoolbox/pkg/compute"
	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
	"gitlab.com/trambi/tournamenttoolbox/pkg/settings"
)

type settingsAdapter struct {
	Name           string                         `json:"name"`
	Organizers     []string                       `json:"organizers"`
	Rankings       map[string]RankSettingsAdapter `json:"rankings"`
	ComputedFields []ComputedFieldAdapter         `json:"computed_fields"`
}

func settingsFromAdapter(adapter settingsAdapter) settings.Settings {
	settings := settings.Settings{}
	settings.Name = adapter.Name
	settings.Organizers = adapter.Organizers
	settings.Rankings = make(map[string]rank.Settings)
	for key, adapter := range adapter.Rankings {
		settings.Rankings[key] = rankSettingsFromAdapter(adapter)
	}
	settings.ComputedFields = make([]compute.ComputedField, len(adapter.ComputedFields))
	for key, adapter := range adapter.ComputedFields {
		settings.ComputedFields[key] = computedFieldFromAdapter(adapter)
	}
	return settings
}

// FromJSONFile create a Settings from JSON file
func FromJSONFile(settingsPath string) (settings.Settings, error) {
	var settings settings.Settings
	jsonFile, err := os.Open(settingsPath)
	// if we os.Open returns an error then handle it
	if err != nil {
		return settings, fmt.Errorf("unable to read file [%s]", err.Error())
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	return FromJSONBytes(byteValue)
}

func FromJSONBytes(byteValue []byte) (settings.Settings, error) {
	var settings settings.Settings
	var err error
	if json.Valid(byteValue) {
		adapter := settingsAdapter{}
		err = json.Unmarshal(byteValue, &adapter)
		settings = settingsFromAdapter(adapter)
	} else {
		err = fmt.Errorf("content of |%v| is unvalid json", string(byteValue[:]))
	}
	return settings, err

}
