// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package json

import (
	"path/filepath"
	"runtime"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/trambi/tournamenttoolbox/pkg/compute"
	"gitlab.com/trambi/tournamenttoolbox/pkg/rank"
	"gitlab.com/trambi/tournamenttoolbox/pkg/settings"
)

func rankingSettingsForTest() rank.Settings {
	pointsCriteria := rank.Criteria{
		Name: "Points", FieldPrefix: "points",
		GameScoped: true, OpponentScoped: false,
		Accumulator: rank.Sum, Less: rank.LessAsDecimal}
	opponentPointsCriteria := rank.Criteria{
		Name: "Opponents points", FieldPrefix: "points",
		GameScoped: false, OpponentScoped: true,
		Accumulator: rank.Sum, Less: rank.LessAsDecimal}
	tdCriteria := rank.Criteria{
		Name: "TD", FieldPrefix: "td",
		GameScoped: true, OpponentScoped: false,
		Accumulator: rank.Sum, Less: rank.LessAsDecimal}
	return rank.Settings{
		IDColumnName:     "ID",
		IDColumnPrefix:   "id",
		NameColumnName:   "Name",
		NameColumnPrefix: "team",
		Criterias:        []rank.Criteria{pointsCriteria, opponentPointsCriteria, tdCriteria},
	}
}

func settingsForTest() settings.Settings {
	equalClause := compute.EqualClause{Left: "td_1", Right: "td_2"}
	greaterClause1 := compute.GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	greaterClause2 := compute.GreaterClause{ToBeGreater: "td_2", ToBeLesser: "td_1"}
	return settings.Settings{Name: "tournament 1",
		Organizers: []string{"organizer 1"},
		Rankings:   map[string]rank.Settings{"Main": rankingSettingsForTest()},
		ComputedFields: []compute.ComputedField{
			{Name: "td_net_1", DefaultValue: "{td_1}-{td_2}", Statements: []compute.Statement{}},
			{Name: "td_net_2", DefaultValue: "{td_2}-{td_1}", Statements: []compute.Statement{}},
			{
				Name:         "points_1",
				DefaultValue: "0",
				Statements: []compute.Statement{
					{Clause: &equalClause, ValueIfTrue: "1"},
					{Clause: &greaterClause1, ValueIfTrue: "3"},
				}},
			{Name: "points_2", DefaultValue: "0", Statements: []compute.Statement{
				{Clause: &equalClause, ValueIfTrue: "1"},
				{Clause: &greaterClause2, ValueIfTrue: "3"},
			}},
		},
	}
}

func TestFromJSONFile(t *testing.T) {
	expectedSettings := settingsForTest()
	_, filename, _, _ := runtime.Caller(0)
	relPath := filepath.Dir(filename)
	settings, err := FromJSONFile(filepath.Join(relPath, "..", "..", "..", "test", ".tournamenttoolbox.json"))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if diff := cmp.Diff(settings, expectedSettings); diff != "" {
		t.Errorf("settings are different from expected (%v)", diff)
	}
}

func TestFromJSONBytes(t *testing.T) {
	equalClause := compute.EqualClause{Left: "td_1", Right: "td_2"}
	greaterClause1 := compute.GreaterClause{ToBeGreater: "td_1", ToBeLesser: "td_2"}
	expectedSettings := settings.Settings{Name: "tournament 1",
		Organizers: []string{"organizer 1"},
		Rankings: map[string]rank.Settings{"Main": {
			IDColumnName:     "ID",
			IDColumnPrefix:   "id",
			NameColumnName:   "Name",
			NameColumnPrefix: "team",
			Criterias: []rank.Criteria{{
				Name: "Points", FieldPrefix: "points",
				GameScoped: true, OpponentScoped: false,
				Accumulator: rank.Sum, Less: rank.LessAsDecimal}},
		}},
		ComputedFields: []compute.ComputedField{
			{Name: "td_net_1", DefaultValue: "{td_1}-{td_2}", Statements: []compute.Statement{}},
			{
				Name:         "points_1",
				DefaultValue: "0",
				Statements: []compute.Statement{
					{Clause: &equalClause, ValueIfTrue: "1"},
					{Clause: &greaterClause1, ValueIfTrue: "3"},
				}},
		},
	}

	bytesValue := []byte(`{
		"name": "tournament 1",
		"organizers": ["organizer 1"],
		"rankings": {
			"Main": {
				"competitor_name_field_prefix": "team",
				"competitor_id_field_prefix": "id",
				"criterias": [{
					"name": "Points",
					"field_prefix": "points",
					"type": "sum_per_game",
					"order": "descending"
				}]
			}
		},
		"computed_fields": [{
			"name": "td_net_1",
			"default_value": "{td_1}-{td_2}"
			},{
			"name": "points_1",
			"default_value": "0",
			"statements": [{
				"clause": "{td_1}={td_2}",
				"value_if_true": "1"
				},{
				"clause": "{td_1}>{td_2}",
				"value_if_true": "3"
			}]
		}]
	}`)
	settings, err := FromJSONBytes(bytesValue)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	} else if diff := cmp.Diff(settings, expectedSettings); diff != "" {
		t.Errorf("settings (%v) are different from expected (%v)", settings, diff)
	}

}
