// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package pair is based on work of Nicolas Mage, Jean-Manuel Pirao and Bertrand Madet
package pair

import (
	"errors"
	"fmt"
)

// Pairing is the minimal pairing structure
type Pairing struct {
	Opponent1 string
	Opponent2 string
}

type Settings struct {
	UseFinale bool
}

// IsEqual returns true if Opponent1 are equals and Opponent2 are equals
func (pairing Pairing) IsEqual(right Pairing) bool {
	return pairing.Opponent1 == right.Opponent1 && pairing.Opponent2 == right.Opponent2
}

// IsReverseEqual returns true if Opponent1 of one are equals to Opponent2 of the other
func (pairing Pairing) IsReverseEqual(right Pairing) bool {
	return pairing.Opponent1 == right.Opponent2 && pairing.Opponent2 == right.Opponent1
}

// //echo 'Appel pairing ' . $this->show2DTab($paired) . ' / ' . $this->showLinearTab($toPaired) . '<br>';
// $nbToPaired = count($toPaired);
// if ( 2 === $nbToPaired ) {
//   if ($this->isAllowed($toPaired[0], $toPaired[1], $constraints)) {
//     $paired[] = array($toPaired[0], $toPaired[1]);
//     return $paired;
//   }
//   return null;
// }

// if( 1 === $nbToPaired){
//   return $paired;
// }

// $roster = $toPaired[0];
// for ($i = 1; $i < $nbToPaired; $i++) {
//   if ( $this->isAllowed($roster, $toPaired[$i], $constraints) ) {
//     $pair = array($roster, $toPaired[$i]);

//     //echo 'Pair ' . $this->showLinearTab($pair) . '<br>';
//     $paired[] = $pair;

//     // on crée un nouveau tableau sans $tab[0] ni $tab[i]
//     $newToPaired = $this->removeValuesByKeys($toPaired, array(0, $i));

//     $tab_pairs = $this->pairing($paired, $newToPaired, $constraints);
//     if ( null === $tab_pairs ){

//       // previous pairing failed, remove current pairing
//       $paired = $this->removeValuesByKeys($paired, array(count($paired)-1));

//       continue;
//     }
//     return $tab_pairs;
//   }
// }
// return null;

func removeIndex(s []string, index int) ([]string, error) {
	if index < 0 || index >= len(s) {
		return s, errors.New("Index out of range")
	}
	ret := make([]string, 0)
	ret = append(ret, s[:index]...)
	return append(ret, s[index+1:]...), nil
}

// Pair return an array of Pairing
func Pair(toBePaired []string, forbiddenPairings []Pairing) ([]Pairing, error) {
	return pair(toBePaired, []Pairing{}, forbiddenPairings)
}

func pair(toBePaired []string, alreadyPaired []Pairing, forbiddenPairings []Pairing) ([]Pairing, error) {
	fmt.Printf("toBePaired :%v\n", toBePaired)
	target := toBePaired[0]
	for i, opponent := range toBePaired[1:] {
		candidate := Pairing{target, opponent}
		if allowed(candidate, forbiddenPairings) {
			alreadyPaired = append(alreadyPaired, candidate)
			newToBePaired, _ := removeIndex(toBePaired[1:], i)
			if len(newToBePaired) < 2 {
				return alreadyPaired, nil
			}
			paired, err := pair(newToBePaired, alreadyPaired, forbiddenPairings)
			if err == nil {
				return paired, nil
			}
			// we can not pair
			alreadyPaired = alreadyPaired[:len(alreadyPaired)-1]
		}
	}
	return alreadyPaired, errors.New("Unable to finish pairing")

}

func allowed(pairing Pairing, forbidden []Pairing) bool {
	for _, notAllowed := range forbidden {
		if pairing.IsEqual(notAllowed) || pairing.IsReverseEqual(notAllowed) == true {
			return false
		}
	}
	return true
}
