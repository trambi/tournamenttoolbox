// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslangpivak.com
package pair

import (
	"errors"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsEqual(t *testing.T) {
	left := Pairing{"a", "b"}
	if left.IsEqual(Pairing{"a", "b"}) == false {
		t.Errorf("Two identical pairings should be equals")
	}
	if left.IsEqual(Pairing{"b", "a"}) == true {
		t.Errorf("Two reverse pairings should not be equals")
	}
	if left.IsEqual(Pairing{"a", "a"}) == true {
		t.Errorf("Two different pairings should not be equals")
	}
	if left.IsEqual(Pairing{"b", "b"}) == true {
		t.Errorf("Two different pairings should not be equals")
	}
}

func TestIsReverseEqual(t *testing.T) {
	left := Pairing{"a", "b"}
	if left.IsReverseEqual(Pairing{"a", "b"}) == true {
		t.Error("Two identical pairings should not be reverse equals")
	}
	if left.IsReverseEqual(Pairing{"b", "a"}) == false {
		t.Error("Two reverse pairings should be reserve equals")
	}
	if left.IsReverseEqual(Pairing{"a", "a"}) == true {
		t.Error("Two different pairings should not be reserve equals")
	}
	if left.IsReverseEqual(Pairing{"b", "b"}) == true {
		t.Error("Two different pairings should not be reserve equals")
	}
}

func TestAllowed(t *testing.T) {
	if allowed(Pairing{"a", "b"}, []Pairing{}) == false {
		t.Error("Without forbidden pairing any pairing should be allowed")
	}
	if allowed(Pairing{"a", "b"}, []Pairing{{"a", "b"}}) == true {
		t.Error("With only current pairing as forbidden pairing should not be allowed")
	}
	if allowed(Pairing{"a", "b"}, []Pairing{{"a", "b"}, {"c", "d"}}) == true {
		t.Error("Current pairing is in forbidden pairing should not be allowed")
	}
	if allowed(Pairing{"a", "b"}, []Pairing{{"c", "d"}, {"a", "b"}}) == true {
		t.Error("Current pairing is in forbidden pairing should not be allowed")
	}
	if allowed(Pairing{"a", "b"}, []Pairing{{"a", "d"}, {"c", "b"}}) == false {
		t.Error("Current pairing is not in forbidden pairing should be allowed")
	}
}

func TestRemoveIndex(t *testing.T) {
	tables := []struct {
		array         []string
		index         int
		expected      []string
		expectedError error
	}{
		{[]string{"0", "1"}, 0, []string{"1"}, nil},
		{[]string{"0", "1"}, 1, []string{"0"}, nil},
		{[]string{"0", "1"}, 2, []string{"0", "1"}, errors.New("Index out of range")},
		{[]string{"0", "1"}, -1, []string{"0", "1"}, errors.New("Index out of range")},
	}
	for _, table := range tables {
		result, err := removeIndex(table.array, table.index)
		if cmp.Equal(result, table.expected) == false {
			t.Errorf("removeIndex(%v,%d) was incorrect, got: %v, want: %v",
				table.array, table.index, result, table.expected)
		}
		if table.expectedError == nil && err != nil {
			t.Errorf("removeIndex(%v,%d) return unexpected error: %v, want: nil",
				table.array, table.index, err.Error())
		}
		if table.expectedError != nil && err == nil {
			t.Errorf("removeIndex(%v,%d) returns error: nil, want: %v",
				table.array, table.index, table.expectedError.Error())
		}
		if table.expectedError != nil && err != nil && table.expectedError.Error() != err.Error() {
			t.Errorf("removeIndex(%v,%d) returns error: %v, want: %v",
				table.array, table.index, err.Error(), table.expectedError.Error())
		}
	}
}

func TestPairFreely(t *testing.T) {
	fourToPair := []string{"1", "2", "3", "4"}
	expected := []Pairing{{"1", "2"}, {"3", "4"}}
	result, err := Pair(fourToPair, []Pairing{})
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
	fiveToPair := []string{"1", "2", "3", "4", "5"}
	result, err = Pair(fiveToPair, []Pairing{})
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}
}

func TestPairWithForbiddenPairings(t *testing.T) {
	fourToPair := []string{"1", "2", "3", "4"}
	forbidden := []Pairing{{"1", "2"}, {"3", "4"}}
	expected := []Pairing{{"1", "3"}, {"2", "4"}}
	result, err := Pair(fourToPair, forbidden)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}

	forbidden = []Pairing{{"1", "2"}, {"3", "4"}, {"1", "3"}, {"2", "4"}}
	expected = []Pairing{{"1", "4"}, {"2", "3"}}
	result, err = Pair(fourToPair, forbidden)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}

	forbidden = []Pairing{{"1", "2"}, {"3", "4"}, {"1", "3"}, {"1", "4"}}
	expectedError := "Unable to finish pairing"
	_, err = Pair(fourToPair, forbidden)
	if err == nil {
		t.Errorf("should return an error: %v\n", err.Error())
	}
	if err.Error() != expectedError {
		t.Errorf("should return error:%v got:%v", expectedError, err.Error())
	}

	forbidden = []Pairing{{"3", "4"}}
	expected = []Pairing{{"1", "3"}, {"2", "4"}}
	result, err = Pair(fourToPair, forbidden)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}

	sixToPair := []string{"1", "2", "3", "4", "5", "6"}
	forbidden = []Pairing{{"5", "6"}, {"4", "6"}, {"6", "3"}}
	expected = []Pairing{{"1", "3"}, {"2", "6"}, {"4", "5"}}
	result, err = Pair(sixToPair, forbidden)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Errorf("result %v is different from expected %v", result, expected)
	}

}

func notTestPairWithThousand(t *testing.T) {
	n := 5_000
	input := []string{}
	expected := []Pairing{}
	forbidden := []Pairing{}
	for i := 0; i < n; i++ {
		input = append(input, strconv.Itoa(i))
		modulo4 := i % 4
		if modulo4 == 1 || modulo4 == 3 {
			forbidden = append(forbidden, Pairing{strconv.Itoa(i - 1), strconv.Itoa(i)})
		}
		if modulo4 == 2 || modulo4 == 3 {
			expected = append(expected, Pairing{strconv.Itoa(i - 2), strconv.Itoa(i)})
		}
	}
	result, err := Pair(input, forbidden)
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err.Error())
	}
	if cmp.Equal(result, expected) != true {
		t.Error("result is different from expected")
	}

}
