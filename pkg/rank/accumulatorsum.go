// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rank

import (
	"math/big"
	"strings"
)

// Sum is the basic Accumulator function
func Sum(left string, right string) string {
	leftInFloat := new(big.Float)
	rightInFloat := new(big.Float)
	_, _, _ = leftInFloat.Parse(strings.Trim(left, " "), 10)
	_, _, _ = rightInFloat.Parse(strings.Trim(right, " "), 10)
	return leftInFloat.Add(leftInFloat, rightInFloat).String()
}
