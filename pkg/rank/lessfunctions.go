// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rank

import (
	"math/big"
	"strings"
)

// LessAsDecimal returns true if left is lesser than right
func LessAsDecimal(left string, right string) bool {
	leftInFloat := new(big.Float)
	leftInFloat.Parse(strings.Trim(left, " "), 10)
	rightInFloat := new(big.Float)
	rightInFloat.Parse(strings.Trim(right, " "), 10)
	return leftInFloat.Cmp(rightInFloat) == -1
}

// ReverseLessAsDecimal returns true if left is greater than right
func ReverseLessAsDecimal(left string, right string) bool {
	leftInFloat := new(big.Float)
	leftInFloat.Parse(strings.Trim(left, " "), 10)
	rightInFloat := new(big.Float)
	rightInFloat.Parse(strings.Trim(right, " "), 10)
	return leftInFloat.Cmp(rightInFloat) == 1
}
