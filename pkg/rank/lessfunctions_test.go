// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rank

import (
	"testing"
)

func TestLessAsInt(t *testing.T) {
	tables := []struct {
		left     string
		right    string
		expected bool
	}{
		{"0", "1", true},
		{"1", "0", false},
		{"1", "1", false},
		{"1", "1.5", true},
		{"1.5", "1.5", false},
	}
	for _, table := range tables {
		result := LessAsDecimal(table.left, table.right)
		if result != table.expected {
			t.Errorf("LessAsInt (%v,%v) was incorrect, got: %v, want: %v.", table.left, table.right, result, table.expected)
		}
	}
}

func TestReverseLessAsInt(t *testing.T) {
	tables := []struct {
		left     string
		right    string
		expected bool
	}{
		{"0", "1", false},
		{"1", "0", true},
		{"1", "1", false},
		{"1.5", "1", true},
	}
	for _, table := range tables {
		result := ReverseLessAsDecimal(table.left, table.right)
		if result != table.expected {
			t.Errorf("ReverseLessAsInt(%v,%v) was incorrect, got: %v, want: %v.", table.left, table.right, result, table.expected)
		}
	}
}
