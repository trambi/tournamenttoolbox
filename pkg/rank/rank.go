// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rank

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

// Rank returns computed rankings given settings and games
func Rank(settings Settings, games [][]string) (Ranking, error) {
	ranking := Ranking{Settings: settings, Rows: make([]RankingRow, 0)}
	if len(games) == 0 {
		return ranking, errors.New("games must have at least header line")
	}
	if len(games) == 1 {
		return ranking, nil
	}
	//First step: we init settings with header
	err := ranking.Settings.Init(games[0])
	if err != nil {
		return ranking, fmt.Errorf("error during initialisation of ranking with first line as headers: %v", err.Error())
	}
	//Second step: create a RankingRow foreach id
	columnNumber := len(ranking.Settings.Criterias)
	rowByID := make(map[string]RankingRow)
	opponentsByID := make(map[string][]string)
	var existInMap bool
	for _, game := range games[1:] {

		updateRows := [2]RankingRow{}
		id1 := strings.Trim(game[ranking.Settings.Indexes.ID[0]], " ")
		id2 := strings.Trim(game[ranking.Settings.Indexes.ID[1]], " ")
		opponentsByID[id1] = append(opponentsByID[id1], id2)
		opponentsByID[id2] = append(opponentsByID[id2], id1)
		updateRows[0], existInMap = rowByID[id1]
		if existInMap == false {
			updateRows[0].ID = id1
			updateRows[0].Name = strings.Trim(game[ranking.Settings.Indexes.Name[0]], " ")
			updateRows[0].Values = make([]string, columnNumber)
		}
		updateRows[1], existInMap = rowByID[id2]
		if existInMap == false {
			updateRows[1].ID = id2
			updateRows[1].Name = strings.Trim(game[ranking.Settings.Indexes.Name[1]], " ")
			updateRows[1].Values = make([]string, columnNumber)
		}

		for index, criteria := range ranking.Settings.Criterias {
			if criteria.GameScoped {
				criteriaIndexes := ranking.Settings.Indexes.Criterias[index]
				for i := 0; i < 2; i++ {
					accumulation := criteria.Accumulator(updateRows[i].Values[index], game[criteriaIndexes[i]])
					updateRows[i].Values[index] = accumulation
				}
			}
		}
		rowByID[id1] = updateRows[0]
		rowByID[id2] = updateRows[1]
	}
	// Third step: we update
	for id, value := range rowByID {
		for index, criteria := range ranking.Settings.Criterias {
			if criteria.OpponentScoped {
				IndexInOpponent := ranking.Settings.Indexes.Criterias[index]
				for _, idOpponent := range opponentsByID[id] {
					previous := value.Values[index]
					current := rowByID[idOpponent].Values[IndexInOpponent[0]]
					value.Values[index] = criteria.Accumulator(previous, current)
				}
			}
		}
		ranking.Rows = append(ranking.Rows, value)
	}
	// Forth step: sort array by criterias
	sort.Sort(sort.Reverse(ranking))
	return ranking, nil
}
