// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rank

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestRankWithoutgame(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{}
	_, err := Rank(settings, games)
	if err == nil {
		t.Error("Expected err to be not nil")
	}
	expected := "games must have at least header line"
	if err.Error() != expected {
		t.Errorf("Expected \"%v\" is different from \"%v\"", expected, err.Error())
	}
}

func TestRankOnlyHeader(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{{"team_1", "points_1", "td_1", "team_2", "points_2", "td_2"}}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Error("Expected err should be nil")
	}
	if ranking.Len() != 0 {
		t.Errorf("Ranking should be empty. Got array of length: %v", ranking.Len())
	}
}

func WithDecimal(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", "3", "1", "2", "second_team", "0", "0"}}
	expectedRows := []RankingRow{
		{"first_team", "1", []string{"3", "1"}},
		{"second_team", "2", []string{"0", "0"}},
	}

	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		fmt.Printf("expectedRows: %v\n", expectedRows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking.Rows is not equal to expectedRows")
	}
}

func TestRankOnegameWithDecimal(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", "0.5", "1", "2", "second_team", "0", "0"}}
	expectedRows := []RankingRow{
		{"first_team", "1", []string{"0.5", "1"}},
		{"second_team", "2", []string{"0", "0"}},
	}

	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		fmt.Printf("expectedRows: %v\n", expectedRows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking.Rows is not equal to expectedRows")
	}
}

func TestRankTrimIDAndName(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", " first_team", "3", "2", "2", "second_team ", "0", "1"},
		{"1 ", "first_team", "1", "1", " 2", "second_team", "1", "1"}}
	expectedRows := []RankingRow{
		{"first_team", "1", []string{"4", "3"}},
		{"second_team", "2", []string{"1", "2"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		t.Errorf("Ranking.Rows %v is not equal to expectedRows %v", ranking.Rows, expectedRows)
	}
}

func TestRankTrimValues(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", " 3", "2 ", "2", "second_team", "0", " 1"}}
	expectedRows := []RankingRow{
		{"first_team", "1", []string{"3", "2"}},
		{"second_team", "2", []string{"0", "1"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		t.Errorf("Ranking.Rows %v is not equal to expectedRows %v", ranking.Rows, expectedRows)
	}
}

func TestRankAddValues(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", "3", "1", "2", "second_team", "0", "0"},
		{"1", "first_team", "1", "1", "2", "second_team", "1", "1"}}
	expectedRows := []RankingRow{
		{"first_team", "1", []string{"4", "2"}},
		{"second_team", "2", []string{"1", "1"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		fmt.Printf("expectedRows: %v\n", expectedRows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking.Rows is not equal to expectedRows")
	}
}

func TestRankIntSorting(t *testing.T) {
	settings := settingsForTest()
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"},
		{"1", "first_team", "10", "1", "2", "second_team", "0", "0"},
		{"1", "first_team", "5", "1", "2", "second_team", "5", "1"}}

	expectedRows := []RankingRow{
		{"first_team", "1", []string{"15", "2"}},
		{"second_team", "2", []string{"5", "1"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		fmt.Printf("expectedRanking.Rows: %v\n", expectedRows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking.Rows is not equal to expectedRows")
	}
}

func TestRankAscSorting(t *testing.T) {
	settings := settingsForTest()
	settings.Criterias[1].Name = "Defense"
	settings.Criterias[1].FieldPrefix = "td_against"
	settings.Criterias[1].Less = ReverseLessAsDecimal
	games := [][]string{
		{"id_1", "team_1", "points_1", "td_against_1", "id_2", "team_2", "points_2", "td_against_2"},
		{"1", "first_team", "3", "0", "2", "third_team", "0", "1"},
		{"3", "second_team", "3", "1", "4", "last_team", "0", "2"}}

	expectedRows := []RankingRow{
		{"first_team", "1", []string{"3", "0"}},
		{"second_team", "3", []string{"3", "1"}},
		{"third_team", "2", []string{"0", "1"}},
		{"last_team", "4", []string{"0", "2"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRows, ranking.Rows) == false {
		fmt.Printf("expectedRows: %v\n", expectedRows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking.Rows is not equal to expectedRows")
	}
}

func TestRankWithOpponentScopeCriteria(t *testing.T) {
	settings := settingsForTest()
	settings.Criterias[1].Name = "Opponent points"
	settings.Criterias[1].FieldPrefix = "points"
	settings.Criterias[1].GameScoped = false
	settings.Criterias[1].OpponentScoped = true

	games := [][]string{
		{"id_1", "team_1", "points_1", "id_2", "team_2", "points_2"},
		{"A", "first_team", "2", "B", "forth_team", "0"},
		{"C", "third_team", "2", "D", "last_team", "0"},
		{"E", "second_team", "2", "F", "fifth_team", "0"},
		{"A", "first_team", "2", "C", "third_team", "0"},
		{"E", "second_team", "2", "B", "forth_team", "0"},
		{"F", "fifth_team", "2", "D", "last_team", "0"},
		{"A", "first_team", "1", "E", "second_team", "1"},
		{"C", "third_team", "2", "F", "fifth_team", "0"},
		{"B", "forth_team", "2", "D", "last_team", "0"},
	}

	expectedRanking := rankingForTest()
	expectedRanking.Rows = []RankingRow{
		{"first_team", "A", []string{"5", "11"}},
		{"second_team", "E", []string{"5", "9"}},
		{"third_team", "C", []string{"4", "7"}},
		{"forth_team", "B", []string{"2", "10"}},
		{"fifth_team", "F", []string{"2", "9"}},
		{"last_team", "D", []string{"0", "8"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRanking.Rows, ranking.Rows) == false {
		fmt.Printf("expectedRanking.Rows: %v\n", expectedRanking.Rows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking is not equal to expectedRanking")
	}
}

func TestRankWithOpponentScopeCriteriaWithDecimal(t *testing.T) {
	settings := settingsForTest()
	settings.Criterias[1].Name = "Opponent points"
	settings.Criterias[1].FieldPrefix = "points"
	settings.Criterias[1].GameScoped = false
	settings.Criterias[1].OpponentScoped = true

	games := [][]string{
		{"id_1", "team_1", "points_1", "id_2", "team_2", "points_2"},
		{"A", "first_team", "1", "B", "forth_team", "0"},
		{"C", "third_team", "1", "D", "last_team", "0"},
		{"E", "second_team", "1", "F", "fifth_team", "0"},
		{"A", "first_team", "1", "C", "third_team", "0"},
		{"E", "second_team", "1", "B", "forth_team", "0"},
		{"F", "fifth_team", "1", "D", "last_team", "0"},
		{"A", "first_team", "0.5", "E", "second_team", "0.5"},
		{"C", "third_team", "1", "F", "fifth_team", "0"},
		{"B", "forth_team", "1", "D", "last_team", "0"},
	}

	expectedRanking := rankingForTest()
	expectedRanking.Rows = []RankingRow{
		{"first_team", "A", []string{"2.5", "5.5"}},
		{"second_team", "E", []string{"2.5", "4.5"}},
		{"third_team", "C", []string{"2", "3.5"}},
		{"forth_team", "B", []string{"1", "5"}},
		{"fifth_team", "F", []string{"1", "4.5"}},
		{"last_team", "D", []string{"0", "4"}},
	}
	ranking, err := Rank(settings, games)
	if err != nil {
		t.Errorf("Expected err to be nil. Got %s", err.Error())
	}
	if cmp.Equal(expectedRanking.Rows, ranking.Rows) == false {
		fmt.Printf("expectedRanking.Rows: %v\n", expectedRanking.Rows)
		fmt.Printf("ranking.Rows: %v\n", ranking.Rows)
		t.Error("Ranking is not equal to expectedRanking")
	}
}
