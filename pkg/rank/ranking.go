// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rank

import (
	"fmt"
	"strings"
)

// Criteria is an element of ranking
type Criteria struct {
	Name           string
	FieldPrefix    string
	GameScoped     bool
	OpponentScoped bool
	Accumulator    func(accumulator string, value string) string
	Less           func(left string, right string) bool
}

type indexes struct {
	ID        [2]int
	Name      [2]int
	Criterias [][]int
}

// Settings is internal ranking settings
// with columnSetting associated with each of Criteria
type Settings struct {
	// IDColumnName is name of the column indicating the identifier of the be ranked
	IDColumnName string
	//IDColumnPrefix is the prefix the id column header
	IDColumnPrefix string
	// NameColumnName is the name of column indicating the name of the be ranked
	NameColumnName string
	//NameColumnPrefix is the prefix the name column header
	NameColumnPrefix string
	Criterias        []Criteria
	Indexes          indexes
}

// RankingRow is a row of ranking
type RankingRow struct {
	Name   string
	ID     string
	Values []string
}

// Ranking is settings and Rows
type Ranking struct {
	Settings Settings
	Rows     []RankingRow
}

func (r Ranking) Len() int {
	return len(r.Rows)
}

func (r Ranking) Swap(i, j int) {
	r.Rows[i], r.Rows[j] = r.Rows[j], r.Rows[i]
}

func (r Ranking) Less(i, j int) bool {
	leftValues, rightValues := r.Rows[i].Values, r.Rows[j].Values
	var valuesLength int
	if len(leftValues) < len(rightValues) {
		valuesLength = len(leftValues)
	} else {
		valuesLength = len(rightValues)
	}

	for i := 0; i < valuesLength; i++ {
		localLess := r.Settings.Criterias[i].Less
		if localLess(leftValues[i], rightValues[i]) {
			return true
		} else if localLess(rightValues[i], leftValues[i]) {
			return false
		}
	}
	return r.Rows[i].Name > r.Rows[j].Name
}

func (s *Settings) initID(headers []string) error {
	s.IDColumnName = "ID"
	id1 := s.IDColumnPrefix + "_1"
	id2 := s.IDColumnPrefix + "_2"
	found1 := false
	found2 := false
	for index, header := range headers {
		trimedHeader := strings.Trim(header, " ")
		if trimedHeader == id1 {
			s.Indexes.ID[0] = index
			found1 = true
		} else if trimedHeader == id2 {
			s.Indexes.ID[1] = index
			found2 = true
		}
	}
	if found1 == false {
		return fmt.Errorf("%s column not find", id1)
	}
	if found2 == false {
		return fmt.Errorf("%s column not find", id2)
	}
	return nil
}

func (s *Settings) initName(header []string) error {
	name1 := s.NameColumnPrefix + "_1"
	name2 := s.NameColumnPrefix + "_2"
	found1 := false
	found2 := false
	for index, value := range header {
		trimedValue := strings.Trim(value, " ")
		if trimedValue == name1 {
			found1 = true
			s.Indexes.Name[0] = index
		} else if trimedValue == name2 {
			s.Indexes.Name[1] = index
			found2 = true
		}
	}
	if found1 == false {
		return fmt.Errorf("%s column not find", name1)
	}
	if found2 == false {
		return fmt.Errorf("%s column not find", name2)
	}
	return nil
}

func (s *Settings) initGameScopedCriterias(headers []string) error {
	for i, criteria := range s.Criterias {
		if criteria.GameScoped {
			s.Indexes.Criterias[i] = make([]int, 2)
			field1 := criteria.FieldPrefix + "_1"
			field2 := criteria.FieldPrefix + "_2"
			found1 := false
			found2 := false
			for j, header := range headers {
				columnName := strings.Trim(header, " ")
				if columnName == field1 {
					s.Indexes.Criterias[i][0] = j
					found1 = true
				} else if columnName == field2 {
					s.Indexes.Criterias[i][1] = j
					found2 = true
				}
			}
			if found1 == false {
				return fmt.Errorf("%s column not find", field1)
			}
			if found2 == false {
				return fmt.Errorf("%s column not find", field2)
			}
		}
	}
	return nil
}

func (s *Settings) initOpponentScopedCriterias() error {
	for i, criteria := range s.Criterias {
		if criteria.OpponentScoped {
			s.Indexes.Criterias[i] = make([]int, 1)
			found := false
			for lookedIndex, lookedCriteria := range s.Criterias {
				if lookedCriteria.OpponentScoped == false &&
					lookedCriteria.FieldPrefix == criteria.FieldPrefix {
					s.Indexes.Criterias[i][0] = lookedIndex
					found = true
				}
			}
			if found == false {
				return fmt.Errorf("%s column not find", criteria.FieldPrefix)
			}
		}
	}
	return nil
}

// Init initialize Settings with headers
func (s *Settings) Init(headers []string) error {
	err := s.initID(headers)
	if err != nil {
		return err
	}
	err = s.initName(headers)
	if err != nil {
		return err
	}
	s.Indexes.Criterias = make([][]int, len(s.Criterias))
	err = s.initGameScopedCriterias(headers)
	if err != nil {
		return err
	}
	err = s.initOpponentScopedCriterias()
	if err != nil {
		return err
	}
	return nil
}

// Equal returns true if Criteria is equal to right
func (left Criteria) Equal(right Criteria) bool {
	if left.Name != right.Name {
		return false
	}
	if left.FieldPrefix != right.FieldPrefix {
		return false
	}
	if left.GameScoped != right.GameScoped {
		return false
	}
	if left.OpponentScoped != right.OpponentScoped {
		return false
	}
	if left.differInLess(right) {
		return false
	}
	if left.differInAccumulator(right) {
		return false
	}
	return true
}

func (left Criteria) differInAccumulator(right Criteria) bool {
	if (left.Accumulator == nil && right.Accumulator != nil) ||
		(left.Accumulator != nil && right.Accumulator == nil) {
		return true
	} else if left.Accumulator != nil && nil != right.Accumulator {
		if left.Accumulator("1", "0") != right.Accumulator("1", "0") {
			return true
		}
	}
	return false
}

func (left Criteria) differInLess(right Criteria) bool {
	if left.Less != nil && nil != right.Less {
		if left.Less("1", "0") != right.Less("1", "0") ||
			left.Less("1", "1") != right.Less("1", "1") {
			return true
		}
	} else if (left.Less == nil && right.Less != nil) ||
		(left.Less != nil && right.Less == nil) {
		return true
	}
	return false
}
