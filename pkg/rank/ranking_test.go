// Copyright 2020-2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rank

import (
	"fmt"
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func rankingForTest() Ranking {
	return Ranking{settingsForTest(), []RankingRow{}}
}

func settingsForTest() Settings {
	pointsCriteria := Criteria{"Points", "points", true, false, Sum, LessAsDecimal}
	tdCriteria := Criteria{"TD", "td", true, false, Sum, LessAsDecimal}
	return Settings{
		IDColumnName:     "ID",
		IDColumnPrefix:   "id",
		NameColumnName:   "Name",
		NameColumnPrefix: "team",
		Criterias:        []Criteria{pointsCriteria, tdCriteria},
	}
}

func TestRankingsSort(t *testing.T) {
	s := settingsForTest()
	toBeSorted := Ranking{
		s,
		[]RankingRow{
			{"first", "id1", []string{"3", "2"}},
			{"third", "id2", []string{"0", "1"}},
			{"last", "id3", []string{"0", "0"}},
			{"second", "id4", []string{"3", "1"}},
		},
	}
	expected := []RankingRow{
		{"first", "id1", []string{"3", "2"}},
		{"second", "id4", []string{"3", "1"}},
		{"third", "id2", []string{"0", "1"}},
		{"last", "id3", []string{"0", "0"}},
	}
	sort.Sort(sort.Reverse(toBeSorted))
	if cmp.Equal(expected, toBeSorted.Rows) == false {
		fmt.Printf("expected: %v\n", expected)
		fmt.Printf("toBeSorted.Rows: %v\n", toBeSorted.Rows)
		t.Errorf("sorted is different from expected")
	}
}

func TestSettingsInit(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	expectedIndexes := indexes{
		[2]int{0, 4}, [2]int{1, 5}, [][]int{{2, 6}, {3, 7}},
	}

	err := settings.Init(headers)
	if err != nil {
		t.Errorf("init should not return an error. Got %s", err.Error())
	}
	if cmp.Equal(settings.Indexes, expectedIndexes) == false {
		t.Errorf("Settings.Indexes: %v differs from expected indexes %v", settings.Indexes, expectedIndexes)
	}
}

func TestSettingsInitWithoutColumnOf1(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "team_1", "points_1", "wrong_column_1", "id_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	expected := "td_1 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithoutColumnOf2(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "wrong_column_2"}
	err := settings.Init(headers)
	expected := "td_2 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithSpaceInName(t *testing.T) {
	settings := settingsForTest()
	expectedIndexes := indexes{
		[2]int{0, 4}, [2]int{1, 5}, [][]int{{2, 6}, {3, 7}},
	}

	headers := []string{"id_1", "   team_1    ", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	if err != nil {
		t.Errorf("init should not return an error. Got %s", err.Error())
	}
	if cmp.Equal(settings.Indexes, expectedIndexes) == false {
		t.Errorf("Settings.Indexes: %v differs from expected indexes %v", settings.Indexes, expectedIndexes)
	}
}

func TestSettingsInitWithoutName1(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "notteam_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	expected := "team_1 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithoutName2(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "team_1", "points_1", "td_1", "id_2", "notteam_2", "points_2", "td_2"}
	err := settings.Init(headers)
	expected := "team_2 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithoutID1(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"notid_1", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	expected := "id_1 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithoutID2(t *testing.T) {
	settings := settingsForTest()
	headers := []string{"id_1", "team_1", "points_1", "td_1", "notid_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	expected := "id_2 column not find"
	if err == nil {
		t.Error("init should return an error. Got nil")
	} else if err.Error() != expected {
		t.Errorf("Excepted error \"%v\" got: %v", expected, err.Error())
	}
}

func TestSettingsInitWithSpaceInID(t *testing.T) {
	settings := settingsForTest()
	expectedIndexes := indexes{
		[2]int{0, 4}, [2]int{1, 5}, [][]int{{2, 6}, {3, 7}},
	}
	headers := []string{"   id_1   ", "team_1", "points_1", "td_1", "id_2", "team_2", "points_2", "td_2"}
	err := settings.Init(headers)
	if err != nil {
		t.Errorf("init should not return an error. Got %s", err.Error())
	}
	if cmp.Equal(settings.Indexes, expectedIndexes) == false {
		t.Errorf("Settings.Indexes: %v differs from expected settings %v", settings.Indexes, expectedIndexes)
	}
}

func TestCriteriaEqual(t *testing.T) {
	basic := Criteria{"a", "b", true, false, Sum, LessAsDecimal}
	withUndefinedLess := Criteria{Name: "a", FieldPrefix: "b", GameScoped: true, OpponentScoped: false, Accumulator: Sum}
	withUndefinedAccumulator := Criteria{Name: "a", FieldPrefix: "b", GameScoped: true, OpponentScoped: false, Less: LessAsDecimal}
	concat := func(a, b string) string {
		return a + b
	}
	withConcat := basic
	withConcat.Accumulator = concat
	tables := []struct {
		left        Criteria
		right       Criteria
		result      bool
		explanation string
	}{
		{basic, Criteria{"a", "b", true, false, Sum, LessAsDecimal}, true, "Same struct"},
		{basic, Criteria{"c", "b", true, false, Sum, LessAsDecimal}, false, "Name differs"},
		{basic, Criteria{"a", "c", true, false, Sum, LessAsDecimal}, false, "Field prefix differs"},
		{basic, Criteria{"a", "b", false, false, Sum, LessAsDecimal}, false, "GameScoped differs"},
		{basic, Criteria{"a", "b", true, true, Sum, LessAsDecimal}, false, "OpponentScoped differs"},
		{basic, Criteria{"a", "b", true, false, Sum, ReverseLessAsDecimal}, false, "LessFunction differs"},
		{basic, withUndefinedLess, false, "LessFunction differs"},
		{withUndefinedLess, withUndefinedLess, true, "Same struct"},
		{basic, withUndefinedAccumulator, false, "Accumulator differs"},
		{basic, withConcat, false, "Accumulator differs"},
		{withUndefinedAccumulator, withUndefinedAccumulator, true, "Same struct"},
	}
	for _, x := range tables {
		if x.left.Equal(x.right) != x.result {
			t.Errorf("%v.Equal(%v) should return %v (%s)", x.left, x.right, x.result, x.explanation)
		}
		if x.right.Equal(x.left) != x.result {
			t.Errorf("%v.Equal(%v) should return %v (%s)", x.right, x.left, x.result, x.explanation)
		}
	}
}
