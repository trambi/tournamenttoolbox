#! /bin/bash

PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in ${PKG_LIST}; do
  tmpfile=$(mktemp -p ./tools/cover)
  go test -covermode=count -coverprofile "${tmpfile}" "$package" ;
done
echo "mode: count" > tools/cover/coverage.cov
cat tools/cover/tmp.* | grep -v "\[no test files\]"  | grep -v "mode: count" >> tools/cover/coverage.cov
go tool cover -func=tools/cover/coverage.cov
rm tools/cover/coverage.cov
rm tools/cover/tmp.*
