#! /bin/bash

if [[ -n ${CI_COMMIT_REF_NAME} ]]
then
  readonly path="${CI_PROJECT_DIR:-.}/cmd/version.go"
  sed -r -i "s/[0-9.]+-dev/${CI_COMMIT_REF_NAME}/" "${path}"
fi