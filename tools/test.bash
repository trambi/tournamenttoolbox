#! /bin/bash
PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in ${PKG_LIST}; do
  go test "$package" ;
done
